/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.util.ArrayList;

import java.time.LocalTime;
import java.time.LocalDate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 *
 * Helper class for the MySQL / MariaDB database.
 * Connects to the database, convert database types to java types.
 * Using Regular SQL and Prepared statements.
 * Functions for reading, updating, deleting, and creating new rows.
 * 
 * @author user
 */
public class DatabaseHelper {
    private Connection _connection;

    /**
     * Class Constructor
     */
    public DatabaseHelper() {
        this._connection = _getMySQLConnection();
    }    
    
    /**
     * Connect to a MySQL / MariaDB database.
     * @return Connection, java type representing a database connection
     */
    private Connection _getMySQLConnection() {
        String databaseName = "client_schedule";
        String username = "sqlUser";
        String password = "Passw0rd!";
        
        try {            
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + databaseName, username, password);            
            
            return connection;
        } catch (Exception e) {
            System.out.println("Error connecting to database!\n" + e.getMessage());
            return null;
        }        
    }

    /**
     * Read all rows for the 'countries' table, return only the country names
     * @return list of country names
     */
    public ArrayList<String> getCountryList() {
        ArrayList<String> list = new ArrayList<String>();
        
        var sqlQuery = "SELECT Country FROM countries";
                
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {                
                list.add(rs.getString("Country"));
            }
            
            return list;
        } catch(SQLException e) {
            e.printStackTrace();
        }        
        
        return null;
    }

    /**
     * Read all rows for the 'countries' table, create instances of 'Country' model 
     * @return representation of rows of 'countries' table
     */
    public ArrayList<Country> getCountryTable() { 
        ArrayList<Country> countryRows = new ArrayList<Country>();

        var sqlQuery = "SELECT * FROM countries";
                
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var c = new Country();


                c.setId(rs.getInt("Country_ID"));
                c.setCountry(rs.getString("Country"));
                c.setCreatedBy(rs.getString("Created_By"));
                c.setLastUpdateBy(rs.getString("Last_Updated_By"));

                c.setCreatedOnLocalDate(rs.getDate("Create_Date").toLocalDate());
                c.setCreatedOnLocalTime(rs.getTime("Create_Date").toLocalTime());
                
                c.setUpdatedOnLocalDate(rs.getDate("Last_Update").toLocalDate());
                c.setUpdatedOnLocalTime(rs.getTime("Last_Update").toLocalTime());
                
                countryRows.add(c);
            }
            
            return countryRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }   

    /**
     * Read all rows for the 'first_level_divisions' table, create instances of 'FirstLevelDivision' model 
     * @return representation of rows of 'first_level_divisions' table
     */    
    public ArrayList<FirstLevelDivision> getAllRowsFromFirstLevelDivisionTable() {      
        ArrayList<FirstLevelDivision> firstLevelDivisionRows = new ArrayList<FirstLevelDivision>();

        var sqlQuery = "SELECT * FROM first_level_divisions";
                
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var f = new FirstLevelDivision();


                f.setId(rs.getInt("Division_ID"));
                f.setCountryID(rs.getInt("Country_ID"));
                f.setDivision(rs.getString("Division"));
                f.setCreatedBy(rs.getString("Created_By"));
                f.setLastUpdateBy(rs.getString("Last_Updated_By"));

                f.setCreatedOnLocalDate(rs.getDate("Create_Date").toLocalDate());
                f.setCreatedOnLocalTime(rs.getTime("Create_Date").toLocalTime());
                
                f.setUpdatedOnLocalDate(rs.getDate("Last_Update").toLocalDate());
                f.setUpdatedOnLocalTime(rs.getTime("Last_Update").toLocalTime());
                
                firstLevelDivisionRows.add(f);
            }
            
            return firstLevelDivisionRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    /**
     * Return rows that match desired country
     * @param country String, value stored in the database. Ex: 'Canada'
     * @return list of Customer instances 
     */
    public ArrayList<Customer> getCustomerRowsByCountry(String country) {
        ArrayList<Customer> customerRows = new ArrayList<Customer>();

        var sqlQuery = "SELECT "
                + "cust.Customer_ID, cust.Division_ID, cust.Customer_Name, cust.Address, cust.Postal_Code, cust.Phone, cust.Created_By, cust.Last_Updated_By, cust.Create_Date, cust.Last_Update, "
                + "fld.Division, ctr.Country  "
                + "FROM customers AS cust "
                + "LEFT JOIN first_level_divisions AS fld "
                + "ON cust.Division_ID = fld.Division_ID "                
                + "LEFT JOIN countries AS ctr "
                + "ON ctr.Country_ID = fld.Country_ID "
                + "WHERE ctr.Country = ?";
                
        try {
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, country);
            
            ResultSet rs = ps.executeQuery();            
        
            while(rs.next()) {
                var c = new Customer();


                c.setId(rs.getInt("cust.Customer_ID"));
                c.setDivisionID(rs.getInt("cust.Division_ID"));
                c.setCustomerName(rs.getString("cust.Customer_Name"));
                c.setAddress(rs.getString("cust.Address"));
                c.setPostalCode(rs.getString("cust.Postal_Code"));
                c.setPhoneNumber(rs.getString("cust.Phone"));
                
                c.setDivisionName(rs.getString("fld.Division"));
                c.setCountryName(rs.getString("ctr.Country"));
                
                c.setCreatedBy(rs.getString("cust.Created_By"));
                c.setLastUpdateBy(rs.getString("cust.Last_Updated_By"));

                c.setCreatedOnLocalDate(rs.getDate("cust.Create_Date").toLocalDate());
                c.setCreatedOnLocalTime(rs.getTime("cust.Create_Date").toLocalTime());
                
                c.setUpdatedOnLocalDate(rs.getDate("cust.Last_Update").toLocalDate());
                c.setUpdatedOnLocalTime(rs.getTime("cust.Last_Update").toLocalTime());
                
                customerRows.add(c);
            }
            
            return customerRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;        
    }

    /**
     * Return all rows from the 'contacts' table, but only one column
     * @return list of Contact names
     */
    public ArrayList<String> getContactList() {
        ArrayList<String> list = new ArrayList<String>();
        
        var sqlQuery = "SELECT Contact_Name FROM contacts";
                
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {                
                list.add(rs.getString("Contact_Name"));
            }
            
            return list;
        } catch(SQLException e) {
            e.printStackTrace();
        }        
        
        return null;
    }

    /**
     * Return customer rows that have appointments between the requested start and end date
     * @param start LocalDate
     * @param end LocalDate
     * @return list of customer instances
     */
    public ArrayList<Customer> getCustomerRowsByAppointmentMonth(LocalDate start, LocalDate end) {
        ArrayList<Customer> customerRows = new ArrayList<Customer>();

        String startLower, endUpper;
        startLower = String.format("%s 00:00:00", start.toString());
        endUpper = String.format("%s 23:59:59", end.toString());        
        
        var sqlQuery = "SELECT "
                + "cust.Customer_ID, cust.Division_ID, cust.Customer_Name, cust.Address, cust.Postal_Code, cust.Phone, cust.Created_By, cust.Last_Updated_By, cust.Create_Date, cust.Last_Update, "
                + "fld.Division, ctr.Country  "
                + "FROM customers AS cust "
                + "LEFT JOIN first_level_divisions AS fld "
                + "ON cust.Division_ID = fld.Division_ID "                
                + "LEFT JOIN countries AS ctr "
                + "ON ctr.Country_ID = fld.Country_ID "
                + "LEFT JOIN appointments AS apt "
                + "ON cust.Customer_ID = apt.Customer_ID "
                + "WHERE apt.Start BETWEEN '" + startLower + "' AND '" + endUpper + "' AND " 
                + " apt.End BETWEEN '" + startLower + "' AND '" + endUpper + "'";
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var c = new Customer();


                c.setId(rs.getInt("cust.Customer_ID"));
                c.setDivisionID(rs.getInt("cust.Division_ID"));
                c.setCustomerName(rs.getString("cust.Customer_Name"));
                c.setAddress(rs.getString("cust.Address"));
                c.setPostalCode(rs.getString("cust.Postal_Code"));
                c.setPhoneNumber(rs.getString("cust.Phone"));
                
                c.setDivisionName(rs.getString("fld.Division"));
                c.setCountryName(rs.getString("ctr.Country"));
                
                c.setCreatedBy(rs.getString("cust.Created_By"));
                c.setLastUpdateBy(rs.getString("cust.Last_Updated_By"));

                c.setCreatedOnLocalDate(rs.getDate("cust.Create_Date").toLocalDate());
                c.setCreatedOnLocalTime(rs.getTime("cust.Create_Date").toLocalTime());
                
                c.setUpdatedOnLocalDate(rs.getDate("cust.Last_Update").toLocalDate());
                c.setUpdatedOnLocalTime(rs.getTime("cust.Last_Update").toLocalTime());
                
                customerRows.add(c);
            }
            
            return customerRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Return list of customers with matching appointment types
     * @param type String, for matching the column 'appointments.Type'
     * @return list of customer instances
     */
    public ArrayList<Customer> getCustomerRowsByAppointmentType(String type) {
        ArrayList<Customer> customerRows = new ArrayList<Customer>();

        var sqlQuery = "SELECT "
                + "cust.Customer_ID, cust.Division_ID, cust.Customer_Name, cust.Address, cust.Postal_Code, cust.Phone, cust.Created_By, cust.Last_Updated_By, cust.Create_Date, cust.Last_Update, "
                + "fld.Division, ctr.Country  "
                + "FROM customers AS cust "
                + "LEFT JOIN first_level_divisions AS fld "
                + "ON cust.Division_ID = fld.Division_ID "                
                + "LEFT JOIN countries AS ctr "
                + "ON ctr.Country_ID = fld.Country_ID "
                + "LEFT JOIN appointments AS apt "
                + "ON cust.Customer_ID = apt.Customer_ID "
                + "WHERE apt.Type = ?";
        
        try {
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, type);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                var c = new Customer();

                c.setId(rs.getInt("cust.Customer_ID"));
                c.setDivisionID(rs.getInt("cust.Division_ID"));
                c.setCustomerName(rs.getString("cust.Customer_Name"));
                c.setAddress(rs.getString("cust.Address"));
                c.setPostalCode(rs.getString("cust.Postal_Code"));
                c.setPhoneNumber(rs.getString("cust.Phone"));
                
                c.setDivisionName(rs.getString("fld.Division"));
                c.setCountryName(rs.getString("ctr.Country"));
                
                c.setCreatedBy(rs.getString("cust.Created_By"));
                c.setLastUpdateBy(rs.getString("cust.Last_Updated_By"));

                c.setCreatedOnLocalDate(rs.getDate("cust.Create_Date").toLocalDate());
                c.setCreatedOnLocalTime(rs.getTime("cust.Create_Date").toLocalTime());
                
                c.setUpdatedOnLocalDate(rs.getDate("cust.Last_Update").toLocalDate());
                c.setUpdatedOnLocalTime(rs.getTime("cust.Last_Update").toLocalTime());
                
                customerRows.add(c);
            }
            
            return customerRows;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }

    /**
     * Select all rows from the customers table
     * @return list of customer instances
     */
    public ArrayList<Customer> getAllRowsFromCustomerTable() {        
        ArrayList<Customer> customerRows = new ArrayList<Customer>();

        var sqlQuery = "SELECT "
                + "cust.Customer_ID, cust.Division_ID, cust.Customer_Name, cust.Address, cust.Postal_Code, cust.Phone, cust.Created_By, cust.Last_Updated_By, cust.Create_Date, cust.Last_Update, "
                + "fld.Division, ctr.Country  "
                + "FROM customers AS cust "
                + "LEFT JOIN first_level_divisions AS fld "
                + "ON cust.Division_ID = fld.Division_ID "                
                + "LEFT JOIN countries AS ctr "
                + "ON ctr.Country_ID = fld.Country_ID";
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var c = new Customer();


                c.setId(rs.getInt("cust.Customer_ID"));
                c.setDivisionID(rs.getInt("cust.Division_ID"));
                c.setCustomerName(rs.getString("cust.Customer_Name"));
                c.setAddress(rs.getString("cust.Address"));
                c.setPostalCode(rs.getString("cust.Postal_Code"));
                c.setPhoneNumber(rs.getString("cust.Phone"));
                
                c.setDivisionName(rs.getString("fld.Division"));
                c.setCountryName(rs.getString("ctr.Country"));
                
                c.setCreatedBy(rs.getString("cust.Created_By"));
                c.setLastUpdateBy(rs.getString("cust.Last_Updated_By"));

                c.setCreatedOnLocalDate(rs.getDate("cust.Create_Date").toLocalDate());
                c.setCreatedOnLocalTime(rs.getTime("cust.Create_Date").toLocalTime());
                
                c.setUpdatedOnLocalDate(rs.getDate("cust.Last_Update").toLocalDate());
                c.setUpdatedOnLocalTime(rs.getTime("cust.Last_Update").toLocalTime());
                
                customerRows.add(c);
            }
            
            return customerRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }

    /**
     * Insert a new row into the 'customers' table
     * @param c Instance of Customer model
     */
    public void createCustomerRow(Customer c) {
        // since the row is being created, set both datetime columns
        c.setCreatedOnLocalDate(LocalDate.now());
        c.setCreatedOnLocalTime(LocalTime.now());
        
        c.setUpdatedOnLocalDate(LocalDate.now());
        c.setUpdatedOnLocalTime(LocalTime.now());     
        
        try {
            var sqlQuery = "INSERT INTO customers "
                    + "(Customer_Name, Address, Postal_Code, Phone, Create_Date, Created_By, Last_Update, Last_Updated_By, Division_ID) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, c.getCustomerName());
            ps.setString(2, c.getAddress());
            ps.setString(3, c.getPostalCode());
            ps.setString(4, c.getPhoneNumber());
            ps.setString(5, c.getCreatedOnString());
            ps.setString(6, c.getCreatedBy());
            ps.setString(7, c.getCreatedOnString());
            ps.setString(8, c.getLastUpdatedBy());
            ps.setInt(9, c.getDivisionID());

            int rows = ps.executeUpdate();
            if (rows != 1) {
                System.out.println("Error inserting row!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Update an existing row in the 'customers' table
     * @param id integer, what row column id to update
     * @param c Instance of the customer model
     */
    public void updateCustomerRow(int id, Customer c) {
        // since the row is being updated, update the datetime column
        c.setUpdatedOnLocalDate(LocalDate.now());
        c.setUpdatedOnLocalTime(LocalTime.now());
        
        try {
            var sqlQuery = "UPDATE customers "
                    + "SET Customer_Name = ?, "
                    + "Address = ?, "
                    + "Postal_Code = ?, "
                    + "Phone = ?, "
                    + "Last_Update = ?, "
                    + "Last_Updated_By = ?, "
                    + "Division_ID = ? "
                    + "WHERE Customer_ID = ?";
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, c.getCustomerName());
            ps.setString(2, c.getAddress());
            ps.setString(3, c.getPostalCode());
            ps.setString(4, c.getPhoneNumber());
            ps.setString(5, c.getUpdatedOnString());
            ps.setString(6, c.getLastUpdatedBy());
            ps.setInt(7, c.getDivisionID());
            ps.setInt(8, id);
            
            int rows = ps.executeUpdate();
            if (rows != 1) {
                System.out.println("Error updating row!");
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }        
    }
    
    /**
     * Delete / Remove a row from the 'customers' table
     * @param id integer, row id
     * @return boolean, if the row was successfully deleted or not
     */
    public boolean deleteCustomerByID(int id) {        
        try {
            var sqlQuery = "DELETE FROM customers WHERE Customer_ID = ?";
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setInt(1, id);
            
            int rows = ps.executeUpdate();
            if (rows == 1) {
                return true;
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    /**
     * Return list of appointment rows that match the foreign column 'appointments.Contact_ID' -> 'contacts.Contact_Name'
     * @param name, String the requested contact name
     * @return list of Appointment instances
     */
    public ArrayList<Appointment> getAppointmentRowsByContactName(String name) {
        ArrayList<Appointment> appointmentRows = new ArrayList<Appointment>();

        var sqlQuery = "SELECT "
                + "apt.Appointment_ID, apt.Title, apt.Description, apt.Location, apt.Type, apt.Start, apt.End, apt.Create_Date, apt.Created_By, apt.Last_Update, apt.Last_Updated_By, apt.Customer_ID, apt.User_ID, apt.Contact_ID, "
                + "cust.Customer_Name, cont.Contact_Name, user.User_Name "
                + "FROM appointments AS apt "
                + "LEFT JOIN customers AS cust "
                + "ON cust.Customer_ID = apt.Customer_ID "                
                + "LEFT JOIN users AS user "
                + "ON user.User_ID = apt.User_ID "
                + "LEFT JOIN contacts AS cont "
                + "ON cont.Contact_ID = apt.Contact_ID "
                + "WHERE cont.Contact_Name = ?";                
        
        try {
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, name);
        
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                var a = new Appointment();

                a.setId(rs.getInt("apt.Appointment_ID"));
                a.setTitle(rs.getString("apt.Title"));
                a.setDescription(rs.getString("apt.Description"));
                a.setLocation(rs.getString("apt.Location"));
                a.setType(rs.getString("apt.Type"));
                
                a.setUserName(rs.getString("user.User_Name"));
                a.setCustomerName(rs.getString("cust.Customer_Name"));
                a.setContactName(rs.getString("cont.Contact_Name"));
                
                var startDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Start").toLocalDate(), rs.getTime("apt.Start").toLocalTime());
                var endDT = this._getDateTimeColumnInUTC(rs.getDate("apt.End").toLocalDate(), rs.getTime("apt.End").toLocalTime());

                a.setAppointmentStart(startDT);
                a.setAppointmentEnd(endDT);
                
                a.setCreatedBy(rs.getString("apt.Created_By"));
                a.setLastUpdateBy(rs.getString("apt.Last_Updated_By"));

                var createdOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Create_Date").toLocalDate(), rs.getTime("apt.Create_Date").toLocalTime());
                var updatedOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Last_Update").toLocalDate(), rs.getTime("apt.Last_Update").toLocalTime());
                
                a.setCreatedOn(createdOnDT);
                a.setUpdatedOn(updatedOnDT);
                
                appointmentRows.add(a);
            }
            
            return appointmentRows;        
        } catch (SQLException e) {
            e.printStackTrace();
        }        
        
        return null;
    }    
    
    /**
     * Count the number of rows that match the requested 'appointments.Customer_ID'
     * @param id, integer the row id
     * @return, integer, number of rows
     */
    public int getAppointmentRowsByCustomerID(int id) {
        int numberOfRows = 0;
        
        var sqlQuery = "SELECT apt.Appointment_ID FROM appointments AS apt WHERE Customer_ID = " + id;
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);

            while(rs.next()) {
                numberOfRows += 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return numberOfRows;
    }
    
    /**
     * Insert a new row into the 'appointments' table
     * @param a, Appointment model instance
     */
    public void createAppointmentRow(Appointment a) {
        // since the row is being created, set both datetime columns        
        a.setCreatedOn(LocalDate.now(ZoneOffset.UTC), LocalTime.now(ZoneOffset.UTC));
        a.setUpdatedOn(LocalDate.now(ZoneOffset.UTC), LocalTime.now(ZoneOffset.UTC));
        
        try {
            var sqlQuery = "INSERT INTO appointments "
                    + "(Title, Description, Location, Type, Start, End, Create_Date, Created_By, Last_Update, Last_Updated_By, Customer_ID, User_ID, Contact_ID) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, a.getTitle());
            ps.setString(2, a.getDescription());
            ps.setString(3, a.getLocation());
            ps.setString(4, a.getType());
            ps.setString(5, a.getAppointmentStart().toString());
            ps.setString(6, a.getAppointmentEnd().toString());
            ps.setString(7, a.getCreatedOn().toString());
            ps.setString(8, a.getCreatedBy());
            ps.setString(9, a.getUpdatedOn().toString());
            ps.setString(10, a.getLastUpdatedBy());
            ps.setInt(11, a.getCustomerID());
            ps.setInt(12, a.getUserID());
            ps.setInt(13, a.getContactID());

            int rows = ps.executeUpdate();
            if (rows != 1) {
                System.out.println("Error inserting row!");
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Update an existing row in the 'appointments' table
     * @param id, integer, what row to update
     * @param a, instance of the Appointment model
     */
    public void updateAppointmentRow(int id, Appointment a) {
        // since the row is being updated, update the datetime column
        a.setUpdatedOn(LocalDate.now(ZoneOffset.UTC), LocalTime.now(ZoneOffset.UTC));  

        try {
            var sqlQuery = "UPDATE appointments "
                    + "SET Title = ?, "
                    + "Description = ?, "
                    + "Location = ?, "
                    + "Type = ?, "
                    + "Start = ?, "
                    + "End = ?, "
                    + "Last_Update = ?, "
                    + "Last_Updated_By = ?, "
                    + "Customer_ID = ?, "
                    + "User_ID = ?, "
                    + "Contact_ID = ? "              
                    + "WHERE Appointment_ID = ?";            
                        
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, a.getTitle());
            ps.setString(2, a.getDescription());
            ps.setString(3, a.getLocation());
            ps.setString(4, a.getType());
            ps.setString(5, a.getAppointmentStart().toString());
            ps.setString(6, a.getAppointmentEnd().toString());
            ps.setString(7, a.getUpdatedOn().toString());
            ps.setString(8, a.getLastUpdatedBy());
            ps.setInt(9, a.getCustomerID());
            ps.setInt(10, a.getUserID());
            ps.setInt(11, a.getContactID());
            ps.setInt(12, id);

            int rows = ps.executeUpdate();
            if (rows != 1) {
                System.out.println("Error updating row!");
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Delete / Remove a row in the 'appointments' table
     * @param id, integer, what row id to remove
     * @return boolean, if the row was successfully deleted or not
     */
    public boolean deleteAppointmentByID(int id) {
        try {
            var sqlQuery = "DELETE FROM appointments WHERE Appointment_ID = ?";
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setInt(1, id);
            
            int rows = ps.executeUpdate();
            if (rows == 1) {
                return true;
            }            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    /**
     * Force the database / Java to assume DATETIME columns are in UTC, convert to Epoch seconds
     * 'rs.getDate("apt.Start").toLocalDate()' or '.toLocalTime()' assume the DATETIME column data is in the Local Computer's timezone
     * 
     * Storing the database column in a TIMESTAMP type would be easier
     * @param date
     * @param time
     * @return 
     */
    private LocalDateTime _getDateTimeColumnInUTC(LocalDate date, LocalTime time) {
        var epochSecondsUTC = date.toEpochSecond(time, ZoneOffset.UTC);
        var localDateTime = LocalDateTime.ofEpochSecond(epochSecondsUTC, 0, ZoneOffset.UTC);
        return localDateTime;
    }
    
    /**
     * Return a single row from the 'appointments' table
     * @param id, row id
     * @return instance of Appointment model
     */
    public Appointment getAppointmentRowByID(int id) {
        Appointment a = new Appointment();

        var sqlQuery = "SELECT "
                + "apt.Appointment_ID, apt.Title, apt.Description, apt.Location, apt.Type, apt.Start, apt.End, apt.Create_Date, apt.Created_By, apt.Last_Update, apt.Last_Updated_By, apt.Customer_ID, apt.User_ID, apt.Contact_ID, "
                + "cust.Customer_Name, cont.Contact_Name, user.User_Name "
                + "FROM appointments AS apt "
                + "LEFT JOIN customers AS cust "
                + "ON cust.Customer_ID = apt.Customer_ID "                
                + "LEFT JOIN users AS user "
                + "ON user.User_ID = apt.User_ID "
                + "LEFT JOIN contacts AS cont "
                + "ON cont.Contact_ID = apt.Contact_ID "
                + "WHERE Appointment_ID = " + id;         
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                a.setId(rs.getInt("apt.Appointment_ID"));
                a.setTitle(rs.getString("apt.Title"));
                a.setDescription(rs.getString("apt.Description"));
                a.setLocation(rs.getString("apt.Location"));
                a.setType(rs.getString("apt.Type"));
                
                a.setUserName(rs.getString("user.User_Name"));
                a.setCustomerName(rs.getString("cust.Customer_Name"));
                a.setContactName(rs.getString("cont.Contact_Name"));
                
                var startDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Start").toLocalDate(), rs.getTime("apt.Start").toLocalTime());
                var endDT = this._getDateTimeColumnInUTC(rs.getDate("apt.End").toLocalDate(), rs.getTime("apt.End").toLocalTime());

                a.setAppointmentStart(startDT);
                a.setAppointmentEnd(endDT);
                
                a.setCreatedBy(rs.getString("apt.Created_By"));
                a.setLastUpdateBy(rs.getString("apt.Last_Updated_By"));

                var createdOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Create_Date").toLocalDate(), rs.getTime("apt.Create_Date").toLocalTime());
                var updatedOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Last_Update").toLocalDate(), rs.getTime("apt.Last_Update").toLocalTime());
                
                a.setCreatedOn(createdOnDT);
                a.setUpdatedOn(updatedOnDT);
                
                return a;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }        
        
        return null;
    }
    
    /**
     * Return a list of 'appointment.Type', but sort and remove duplicates. Ex: If two rows contain the type: 'Briefing', 'Briefing' is only returned once.
     * @return list of types
     */
    public ArrayList<String> getDistinctTypeColumnsFromAppointmentTable() {
        ArrayList<String> rows = new ArrayList<String>();
        
        var sqlQuery = "SELECT DISTINCT "
                + "apt.Type "
                + "FROM appointments AS apt";        
        try {
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                rows.add(rs.getString("apt.Type"));
            }
            return rows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    /**
     * Select all the rows from the table 'appointments'
     * @return list of Appointment model instances
     */
    public ArrayList<Appointment> getAllRowsFromAppointmentTable() {    
        ArrayList<Appointment> appointmentRows = new ArrayList<Appointment>();

        var sqlQuery = "SELECT "
                + "apt.Appointment_ID, apt.Title, apt.Description, apt.Location, apt.Type, apt.Start, apt.End, apt.Create_Date, apt.Created_By, apt.Last_Update, apt.Last_Updated_By, apt.Customer_ID, apt.User_ID, apt.Contact_ID, "
                + "cust.Customer_Name, cont.Contact_Name, user.User_Name "
                + "FROM appointments AS apt "
                + "LEFT JOIN customers AS cust "
                + "ON cust.Customer_ID = apt.Customer_ID "                
                + "LEFT JOIN users AS user "
                + "ON user.User_ID = apt.User_ID "
                + "LEFT JOIN contacts AS cont "
                + "ON cont.Contact_ID = apt.Contact_ID";                
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var a = new Appointment();


                a.setId(rs.getInt("apt.Appointment_ID"));
                a.setTitle(rs.getString("apt.Title"));
                a.setDescription(rs.getString("apt.Description"));
                a.setLocation(rs.getString("apt.Location"));
                a.setType(rs.getString("apt.Type"));
                
                a.setUserName(rs.getString("user.User_Name"));
                a.setCustomerName(rs.getString("cust.Customer_Name"));
                a.setContactName(rs.getString("cont.Contact_Name"));
                
                var startDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Start").toLocalDate(), rs.getTime("apt.Start").toLocalTime());
                var endDT = this._getDateTimeColumnInUTC(rs.getDate("apt.End").toLocalDate(), rs.getTime("apt.End").toLocalTime());

                a.setAppointmentStart(startDT);
                a.setAppointmentEnd(endDT);
                
                a.setCreatedBy(rs.getString("apt.Created_By"));
                a.setLastUpdateBy(rs.getString("apt.Last_Updated_By"));

                var createdOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Create_Date").toLocalDate(), rs.getTime("apt.Create_Date").toLocalTime());
                var updatedOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Last_Update").toLocalDate(), rs.getTime("apt.Last_Update").toLocalTime());
                
                a.setCreatedOn(createdOnDT);
                a.setUpdatedOn(updatedOnDT);
                
                appointmentRows.add(a);
            }
            
            return appointmentRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }

    /**
     * Return the appointment rows that have 'appointments.Start' and 'appointments.End' between the requested dates
     * @param start LocalDate, requested beginning date
     * @param end LocalDate, requested ending date
     * @return list of Appointment model instances
     */
    public ArrayList<Appointment> getAppointmentRowsByStartAndEnd(LocalDate start, LocalDate end) {
        ArrayList<Appointment> appointmentRows = new ArrayList<Appointment>();
        
        String startLower, endUpper;
        startLower = String.format("%s 00:00:00", start.toString());
        endUpper = String.format("%s 23:59:59", end.toString());
        
        var sqlQuery = "SELECT "
                + "apt.Appointment_ID, apt.Title, apt.Description, apt.Location, apt.Type, apt.Start, apt.End, apt.Create_Date, apt.Created_By, apt.Last_Update, apt.Last_Updated_By, apt.Customer_ID, apt.User_ID, apt.Contact_ID, "
                + "cust.Customer_Name, cont.Contact_Name, user.User_Name "                
                + "FROM appointments AS apt "         
                + "LEFT JOIN customers AS cust "
                + "ON cust.Customer_ID = apt.Customer_ID "                
                + "LEFT JOIN users AS user "
                + "ON user.User_ID = apt.User_ID "
                + "LEFT JOIN contacts AS cont "
                + "ON cont.Contact_ID = apt.Contact_ID "
                + "WHERE apt.Start BETWEEN '" + startLower + "' AND '" + endUpper + "' AND " 
                + " apt.End BETWEEN '" + startLower + "' AND '" + endUpper + "'";
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var a = new Appointment();


                a.setId(rs.getInt("apt.Appointment_ID"));
                a.setTitle(rs.getString("apt.Title"));
                a.setDescription(rs.getString("apt.Description"));
                a.setLocation(rs.getString("apt.Location"));
                a.setType(rs.getString("apt.Type"));
                
                a.setUserName(rs.getString("user.User_Name"));
                a.setCustomerName(rs.getString("cust.Customer_Name"));
                a.setContactName(rs.getString("cont.Contact_Name"));

                var startDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Start").toLocalDate(), rs.getTime("apt.Start").toLocalTime());
                var endDT = this._getDateTimeColumnInUTC(rs.getDate("apt.End").toLocalDate(), rs.getTime("apt.End").toLocalTime());

                a.setAppointmentStart(startDT);
                a.setAppointmentEnd(endDT);
                
                a.setCreatedBy(rs.getString("apt.Created_By"));
                a.setLastUpdateBy(rs.getString("apt.Last_Updated_By"));

                var createdOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Create_Date").toLocalDate(), rs.getTime("apt.Create_Date").toLocalTime());
                var updatedOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Last_Update").toLocalDate(), rs.getTime("apt.Last_Update").toLocalTime());
                
                a.setCreatedOn(createdOnDT);
                a.setUpdatedOn(updatedOnDT);
                
                appointmentRows.add(a);
            }
            
            return appointmentRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;        
    }

    /**
     * Return the appointment rows that have 'appointments.Start' and 'appointments.End' between the requested datetimes
     * @param start LocalDateTime, requested beginning datetime
     * @param end LocalDateTime, requested ending datetime
     * @return list of Appointment model instances
     */    
    public ArrayList<Appointment> getAppointmentRowsByStartAndEnd(LocalDateTime start, LocalDateTime end) {
        ArrayList<Appointment> appointmentRows = new ArrayList<Appointment>();
        
        var sqlQuery = "SELECT "
                + "apt.Appointment_ID, apt.Title, apt.Description, apt.Location, apt.Type, apt.Start, apt.End, apt.Create_Date, apt.Created_By, apt.Last_Update, apt.Last_Updated_By, apt.Customer_ID, apt.User_ID, apt.Contact_ID, "
                + "cust.Customer_Name, cont.Contact_Name, user.User_Name "                
                + "FROM appointments AS apt "         
                + "LEFT JOIN customers AS cust "
                + "ON cust.Customer_ID = apt.Customer_ID "                
                + "LEFT JOIN users AS user "
                + "ON user.User_ID = apt.User_ID "
                + "LEFT JOIN contacts AS cont "
                + "ON cont.Contact_ID = apt.Contact_ID "
                + "WHERE apt.Start BETWEEN '" + start.toString() + "' AND '" + end.toString() + "'";
        
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var a = new Appointment();


                a.setId(rs.getInt("apt.Appointment_ID"));
                a.setTitle(rs.getString("apt.Title"));
                a.setDescription(rs.getString("apt.Description"));
                a.setLocation(rs.getString("apt.Location"));
                a.setType(rs.getString("apt.Type"));
                
                a.setUserName(rs.getString("user.User_Name"));
                a.setCustomerName(rs.getString("cust.Customer_Name"));
                a.setContactName(rs.getString("cont.Contact_Name"));

                var startDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Start").toLocalDate(), rs.getTime("apt.Start").toLocalTime());
                var endDT = this._getDateTimeColumnInUTC(rs.getDate("apt.End").toLocalDate(), rs.getTime("apt.End").toLocalTime());

                a.setAppointmentStart(startDT);
                a.setAppointmentEnd(endDT);
                
                a.setCreatedBy(rs.getString("apt.Created_By"));
                a.setLastUpdateBy(rs.getString("apt.Last_Updated_By"));

                var createdOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Create_Date").toLocalDate(), rs.getTime("apt.Create_Date").toLocalTime());
                var updatedOnDT = this._getDateTimeColumnInUTC(rs.getDate("apt.Last_Update").toLocalDate(), rs.getTime("apt.Last_Update").toLocalTime());
                
                a.setCreatedOn(createdOnDT);
                a.setUpdatedOn(updatedOnDT);
                
                appointmentRows.add(a);
            }
            
            return appointmentRows;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        
        return null;        
    }
    
    /**
     * Return a list of rows with matching 'users.User_Name'
     * @param username String, the requested username
     * @return an instance of User model, usernames should be unique
     */
    public User getUserRowByUsername(String username) {
        User u = null;

        var sqlQuery = "SELECT * FROM users WHERE User_Name = ? LIMIT 1";
        
        try {
            PreparedStatement ps = this._connection.prepareStatement(sqlQuery);
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
        
            while(rs.next()) {
                u = new User();
                
                u.setId(rs.getInt("User_ID"));
                u.setUsername(rs.getString("User_Name"));
                u.setPassword(rs.getString("Password"));
                
                u.setCreatedBy(rs.getString("Created_By"));
                u.setLastUpdateBy(rs.getString("Last_Updated_By"));
                
                u.setCreatedOnLocalDate(rs.getDate("Create_Date").toLocalDate());
                u.setCreatedOnLocalTime(rs.getTime("Create_Date").toLocalTime());
                
                u.setUpdatedOnLocalDate(rs.getDate("Last_Update").toLocalDate());
                u.setUpdatedOnLocalTime(rs.getTime("Last_Update").toLocalTime());
                
                return u;
            }
            
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return u;
    }
    
    /**
     * Return all rows from the table 'users'
     * @return instances of the model 'User'
     */
    public ArrayList<User> getAllRowsFromUserTable() {        
        ArrayList<User> userRows = new ArrayList<User>();

        var sqlQuery = "SELECT * FROM users";
                
        try {            
            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var u = new User();
                
                u.setId(rs.getInt("User_ID"));
                u.setUsername(rs.getString("User_Name"));
                u.setPassword(rs.getString("Password"));
                
                u.setCreatedBy(rs.getString("Created_By"));
                u.setLastUpdateBy(rs.getString("Last_Updated_By"));
                
                u.setCreatedOnLocalDate(rs.getDate("Create_Date").toLocalDate());
                u.setCreatedOnLocalTime(rs.getTime("Create_Date").toLocalTime());
                
                u.setUpdatedOnLocalDate(rs.getDate("Last_Update").toLocalDate());
                u.setUpdatedOnLocalTime(rs.getTime("Last_Update").toLocalTime());
                
                userRows.add(u);                
            }
            
            return userRows;            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    /**
     * Return all the rows from the table 'contacts'
     * @return list of Contact model instances
     */
    public ArrayList<Contact> getAllRowsFromContactTable() {
        ArrayList<Contact> contactRows = new ArrayList<Contact>();
        
        var sqlQuery = "SELECT * FROM contacts";
        
        try {

            Statement stmt = this._connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlQuery);
        
            while(rs.next()) {
                var c = new Contact();
                
                c.setId(rs.getInt("Contact_ID"));
                c.setName(rs.getString("Contact_Name"));
                c.setEmail(rs.getString("Email"));
                
                contactRows.add(c);
            }
            
            return contactRows;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
}
