/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Database model representing the 'users' table columns.
 * @author user
 */
public class User {
    /*
        DateTime columns are UTC: "Coordinated Universal Time (UTC) is used for storing the time in the database"
    */
    
    private int _id;
    private String _username, _password;
    private String _createdBy, _lastUpdatedBy;
    private LocalDate _createdOnLocalDate, _updatedOnLocalDate;
    private LocalTime _createdOnLocalTime, _updatedOnLocalTime;
    
    public User() {
        
    }
    
    /**
     * Getter function 
     * @return int, row id, 'users.User_ID'
     */
    public int getId() {
        return this._id;       
    }

    /**
     * Getter function 
     * @return String, 'users.User_Name'
     */    
    public String getUsername() {
        return this._username;
    }
    
    /**
     * Getter function 
     * @return String, 'users.Password'
     */
    public String getPassword() {
        return this._password;
    }

    /**
     * Getter function 
     * @return String, 'users.Created_By'
     */    
    public String getCreatedBy() {
        return this._createdBy;
    }


    /**
     * Getter function 
     * @return String, 'users.Updated_By'
     */    
    public String getLastUpdatedBy() {
        return this._lastUpdatedBy;
    }
    
    /**
     * Getter function 
     * @return LocalDate, 'users.Created_On', represents a DATETIME column
     */
    public LocalDate getCreatedOnLocalDate() {
        return this._createdOnLocalDate;
    }

    /**
     * Getter function 
     * @return LocalTime, 'users.Created_On', represents a DATETIME column
     */    
    public LocalTime getCreatedOnLocalTime() {
        return this._createdOnLocalTime;
    }

    /**
     * Getter function 
     * @return LocalDate, 'users.Updated_On', represents a DATETIME column
     */
    public LocalDate getUpdatedOnLocalDate() {
        return this._updatedOnLocalDate;
    }

    /**
     * Getter function 
     * @return LocalTime, 'users.Updated_On', represents a DATETIME column
     */    
    public LocalTime getUpdatedOnLocalTime() {
        return this._updatedOnLocalTime;
    }
    
    /**
     * Setter function
     * @param id integer, row id
     */
    public void setId(int id) {
        this._id = id;
    }
    
    /**
     * Setter function
     * @param username String 
     */
    public void setUsername(String username) {
        this._username = username;
    }

    /**
     * Setter function
     * @param password String 
     */    
    public void setPassword(String password) {
        this._password = password;
    }
    
    /**
     * Setter function
     * @param createdBy String 
     */    
    public void setCreatedBy(String createdBy) {
        this._createdBy = createdBy;
    }
    
    /**
     * Setter function
     * @param lastUpdateBy String 
     */    
    public void setLastUpdateBy(String lastUpdateBy) {
        this._lastUpdatedBy = lastUpdateBy;
    } 
    
    /**
     * Setter function
     * @param createdOn LocalDate
     */
    public void setCreatedOnLocalDate(LocalDate createdOn) {
        this._createdOnLocalDate = createdOn;
    }

    /**
     * Setter function
     * @param createdOn LocalTime
     */    
    public void setCreatedOnLocalTime(LocalTime createdOn) {
        this._createdOnLocalTime = createdOn;
    }

    
    /**
     * Setter function
     * @param updatedOn LocalDate
     */    
    public void setUpdatedOnLocalDate(LocalDate updatedOn) {
        this._updatedOnLocalDate = updatedOn;
    }

    /**
     * Setter function
     * @param updatedOn LocalTime
     */    
    public void setUpdatedOnLocalTime(LocalTime updatedOn) {
        this._updatedOnLocalTime = updatedOn;
    }
}
