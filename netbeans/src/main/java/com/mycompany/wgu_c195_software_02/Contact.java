/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

/**
 * Database model representing the 'contacts' table columns.
 * @author user
 */
public class Contact {
    private int _id;
    private String _name, _email;
    
    /**
     * Class constructor
     */
    public Contact() {
    }
    
    /**
     * Getter function
     * @return integer, row id, 'contacts.Contact_ID'
     */    
    public int getID() {
        return this._id;
    }
    
    /**
     * Getter function
     * @return String, 'contacts.Contact_Name'
     */
    public String getName() {
        return this._name;
    }
    
    /**
     * Getter function
     * @return String, 'contacts.Email'
     */
    public String getEmail() {
        return this._email;
    }
    
    /**
     * Setter function
     * @param id integer row id
     */
    public void setId(int id) {
        this._id = id;
    }
    
    /**
     * Setter function
     * @param name, contact name
     */
    public void setName(String name) {
        this._name = name;
    }
    
    /**
     * Setter function
     * @param email, contact e-mail address
     */
    public void setEmail(String email) {
        this._email = email;
    }
}
