/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;

/**
 *
 * Database model representing the 'appointment' table columns.
 * Contains some data from foreign tables for ease of access in the the JavaFX GUI logic.
 * @author user
 */
public class Appointment {

    private int _id;
    private int _customerID, _userID, _contactID;
    private String _title, _description, _location, _type;    
    private LocalDateTime _appointmentStartUTC, _appointmentEndUTC;
    private String _createdBy, _lastUpdatedBy;
    private LocalDateTime _createdOn, _updatedOn;
    private String _customerName, _userName, _contactName;
    
    public Appointment() {
    }
    
    /**
     * Getter function
     * @return row id, auto-incrementing integer column
     */
    public int getId() {
        return this._id;
    }

    /**
     * Getter function.
     * @return String, customer name, from a foreign key, 'appointment.Customer_ID' -> 'customer.Customer_Name'
     */
    public String getCustomerName() {
        return this._customerName;
    }

    /**
     * Getter function
     * @return String, user name, from a foreign key key, 'appointment.User_ID' -> 'user.User_Name'
     */
    public String getUserName() {
        return this._userName;
    }

    /**
     * Getter function
     * @return String, user name, from a foreign key key, 'appointment.Contact_ID' -> 'contacts.Contact_Name'
     */    
    public String getContactName() {
        return this._contactName;
    }
    
    /**
     * Getter function
     * @return integer, 'appointment.Customer_ID'
     */
    public int getCustomerID() {
        return this._customerID;
    }

    /**
     * Getter function
     * @return integer, 'appointment.User_ID'
     */    
    public int getUserID() {
        return this._userID;
    }

    /**
     * Getter function
     * @return integer, 'appointment.Contact_ID'
     */    
    public int getContactID() {
        return this._contactID;
    }

    /**
     * Getter function
     * @return String, 'appointment.Title'
     */
    public String getTitle() {
        return this._title;
    }

    /**
     * Getter function
     * @return String, 'appointment.Description'
     */    
    public String getDescription() {
        return this._description;
    }

    /**
     * Getter function
     * @return String, 'appointment.Location'
     */    
    public String getLocation() {
        return this._location;
    }
    
    /**
     * Getter function
     * @return String, 'appointment.Type'
     */
    public String getType() {
        return this._type;
    }    
    
    /**
     * Getter function
     * @return String, 'appointment.Created_By'
     */    
    public String getCreatedBy() {
        return this._createdBy;
    }

    /**
     * Getter function
     * @return String, 'appointment.Last_Updated_By'
     */    
    public String getLastUpdatedBy() {
        return this._lastUpdatedBy;
    }
    
    /**
     * Getter function
     * @return LocalDateTime, 'appointment.Start' (DATETIME column)
     */    
    public LocalDateTime getAppointmentStart() {
        return this._appointmentStartUTC;
    }

    /**
     * Getter function
     * @return LocalDateTime, 'appointment.End' (DATETIME column)
     */        
    public LocalDateTime getAppointmentEnd() {
        return this._appointmentEndUTC;
    }    

    /**
     * Getter function
     * @return LocalDateTime, 'appointment.Created_On' (DATETIME column)
     */        
    public LocalDateTime getCreatedOn() {
        return this._createdOn;
    }

    /**
     * Getter function
     * @return LocalDateTime, 'appointment.Updated_On' (DATETIME column)
     */        
    public LocalDateTime getUpdatedOn() {
        return this._updatedOn;
    }

    /**
     * Setter functions
     * @param id row id, auto-incrementing integer
     */
    public void setId(int id) {
        this._id = id;
    }

    /**
     *  Setter function
     * @param name String representing a foreign key, for 'appointment.Customer_ID' -> 'customer.Customer_Name'
     */
    public void setCustomerName(String name) {
        this._customerName = name;
    }

    /**
     *  Setter function
     * @param name String representing a foreign key, for 'appointment.User_ID' -> 'user.User_Name'
     */    
    public void setUserName(String name) {
        this._userName = name;
    }

    /**
     *  Setter function
     * @param name String representing a foreign key, for 'appointment.Contact_ID' -> 'contacts.Contact_Name'
     */    
    public void setContactName(String name) {
        this._contactName = name;
    }    

    /**
     *  Setter function
     * @param id integer represents 'appointment.Customer_ID'
     */    
    public void setCustomerID(int id) {
        this._customerID = id;
    }

    /**
     *  Setter function
     * @param id integer represents 'appointment.User_ID'
     */        
    public void setUserID(int id) {
        this._userID = id;
    }

    /**
     *  Setter function
     * @param id integer represents 'appointment.Contact_ID'
     */        
    public void setContactID(int id) {
        this._contactID = id;
    }

    /**
     *  Setter function
     * @param title String represents 'appointment.Title'
     */
    public void setTitle(String title) {
        this._title = title;
    }

    /**
     *  Setter function
     * @param description String represents 'appointment.Description'
     */    
    public void setDescription(String description) {
        this._description = description;
    }
    
    /**
     *  Setter function
     * @param location String represents 'appointment.Location'
     */    
    public void setLocation(String location) {
        this._location = location;
    }

    /**
     *  Setter function
     * @param type String represents 'appointment.Type'
     */    
    public void setType(String type) {
        this._type = type;
    }    
        
    /**
     *  Setter function
     * @param createdBy String represents 'appointment.Created_By'
     */
    public void setCreatedBy(String createdBy) {
        this._createdBy = createdBy;
    }

    /**
     *  Setter function
     * @param lastUpdateBy String represents 'appointment.Last_Update_By'
     */    
    public void setLastUpdateBy(String lastUpdateBy) {
        this._lastUpdatedBy = lastUpdateBy;
    } 
    
    
    /**
     * Setter function
     * @param date represents a DATETIME column 'appointment.Created_On'
     * @param time represents a DATETIME column 'appointment.Created_On'
     */
    public void setCreatedOn(LocalDate date, LocalTime time) {
        this._createdOn = LocalDateTime.of(date, time);
    }

    /**
     * Overloaded Setter function
     * @param dt represents a DATETIME column 'appointment.Created_On'
     */    
    public void setCreatedOn(LocalDateTime dt) {
        this._createdOn = dt;
    }    

    /**
     * Setter function
     * @param date represents a DATETIME column 'appointment.Updated_On'
     * @param time represents a DATETIME column 'appointment.Updated_On'
     */    
    public void setUpdatedOn(LocalDate date, LocalTime time) {
        this._updatedOn = LocalDateTime.of(date, time);
    }

    /**
     * Overloaded Setter function
     * @param dt represents a DATETIME column 'appointment.Update_On'
     */    
    public void setUpdatedOn(LocalDateTime dt) {
        this._updatedOn = dt;
    }
    
    /**
     * Assume that the LocalDate and LocalTime are in UTC. Timezone Conversion must happen previously!
     * @param date LocalDate, representation of start date
     * @param time LocalTime, representation of start time
     */
    public void setAppointmentStart(LocalDate date, LocalTime time) {
        this._appointmentStartUTC = LocalDateTime.of(date, time);
    }
    
    /**
     * Overload Timezone conversion function with a different type.
     * @param dt LocalDateTime, representation of start date and time
     */
    public void setAppointmentStart(LocalDateTime dt) {
        this._appointmentStartUTC = dt;
    }

    /**
     * Assume that the LocalDate and LocalTime are in UTC. Timezone Conversion must happen previously!
     * @param date LocalDate, representation of end date
     * @param time LocalTime, representation of end time
     */    
    public void setAppointmentEnd(LocalDate date, LocalTime time) {
        this._appointmentEndUTC = LocalDateTime.of(date, time);
    }

    /**
     * Overload function with a different type.
     * @param dt LocalDateTime, representation of end date and time
     */    
    public void setAppointmentEnd(LocalDateTime dt) {
        this._appointmentEndUTC = dt;
    }
}
