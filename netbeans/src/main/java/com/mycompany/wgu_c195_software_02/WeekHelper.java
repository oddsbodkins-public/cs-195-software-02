/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;

/**
 * Helper class for sorting appointments by week. Used in App.java
 * @author user
 */
public class WeekHelper {
    private int _weekNumber;
    private LocalDate _start, _end;
    
    /**
     * Class constructor
     * @param weekNumber week number in the year 1-53
     * @param start LocalDate, starting day of week
     * @param end LocalDate, ending day of week
     */
    public WeekHelper(int weekNumber, LocalDate start, LocalDate end) {
        this._weekNumber = weekNumber;
        this._start = start;
        this._end = end;
    }
    
    /**
     * Getter function
     * @return LocalDate, start of week
     */
    public LocalDate getStart() {
        return this._start;
    }
    
    /**
     * Getter function
     * @return LocalDate, end of week
     */
    public LocalDate getEnd() {
        return this._end;
    }
    
    /**
     * Make a class representation in a string
     * @param weekLabel, Human language translation of word 'Week'
     * @return 
     */
    public String toString(String weekLabel) {        
        return String.format(weekLabel + " %d: '%s' to '%s'", this._weekNumber, this._start.toString(), this._end.toString());
    }
}
