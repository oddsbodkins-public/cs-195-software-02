/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Database model representing the 'customers' table columns.
 * @author user
 */
public class Customer {
    private int _id;
    private int _divisionID;
    private String _divisionName, _countryName;
    private String _customerName, _address, _postalCode, _phoneNumber;
    private String _createdBy, _lastUpdatedBy;    
    private LocalDate _createdOnLocalDate, _updatedOnLocalDate;
    private LocalTime _createdOnLocalTime, _updatedOnLocalTime;    
    
    public Customer() {
        
    }

    /**
     * Getter function for row data.
     * @return row id
     */
    public int getId() {
        return this._id;
    }

    public int getDivisionID() {
        return this._divisionID;
    }
    
    public String getDivisionName() {
        return this._divisionName;
    }
    
    public String getCountryName() {
        return this._countryName;
    }
    
    public String getCustomerName() {
        return this._customerName;
    }

    public String getAddress() {
        return this._address;
    }

    public String getPostalCode() {
        return this._postalCode;
    }

    public String getPhoneNumber() {
        return this._phoneNumber;
    }
    
    public String getCreatedBy() {
        return this._createdBy;
    }

    public String getLastUpdatedBy() {
        return this._lastUpdatedBy;
    }
    
    public LocalDate getCreatedOnLocalDate() {
        return this._createdOnLocalDate;
    }

    public LocalTime getCreatedOnLocalTime() {
        return this._createdOnLocalTime;
    }

    public String getCreatedOnString() {
        return this._createdOnLocalDate.toString() + " " + this._createdOnLocalTime.toString();        
    }    
    
    public LocalDate getUpdatedOnLocalDate() {
        return this._updatedOnLocalDate;
    }

    public LocalTime getUpdatedOnLocalTime() {
        return this._updatedOnLocalTime;
    }

    public String getUpdatedOnString() {
        return this._updatedOnLocalDate.toString() + " " + this._updatedOnLocalTime.toString();
    }
    
    /**
     * Setter function
     * @param id
     */
    public void setId(int id) {
        this._id = id;
    }
    
    public void setDivisionID(int id) {
        this._divisionID = id;
    }
    
    public void setDivisionName(String name) {
        this._divisionName = name;
    }
    
    public void setCountryName(String name) {
        this._countryName = name;
    }
    
    public void setCustomerName(String name) {
        this._customerName = name;
    }

    public void setAddress(String address) {
        this._address = address;
    }
    
    public void setPostalCode(String postalCode) {
        this._postalCode = postalCode;
    }    

    public void setPhoneNumber(String phoneNumber) {
        this._phoneNumber = phoneNumber;
    }
    
    public void setCreatedBy(String createdBy) {
        this._createdBy = createdBy;
    }
    
    public void setLastUpdateBy(String lastUpdateBy) {
        this._lastUpdatedBy = lastUpdateBy;
    } 
    
    public void setCreatedOnLocalDate(LocalDate createdOn) {
        this._createdOnLocalDate = createdOn;
    }
    
    public void setCreatedOnLocalTime(LocalTime createdOn) {
        this._createdOnLocalTime = createdOn;
    }

    public void setUpdatedOnLocalDate(LocalDate updatedOn) {
        this._updatedOnLocalDate = updatedOn;
    }
    
    public void setUpdatedOnLocalTime(LocalTime updatedOn) {
        this._updatedOnLocalTime = updatedOn;
    }


}
