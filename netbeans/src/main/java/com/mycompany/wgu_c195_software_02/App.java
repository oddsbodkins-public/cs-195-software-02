package com.mycompany.wgu_c195_software_02;

import javafx.application.Application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Toggle;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.control.PasswordField;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Separator;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import javafx.scene.paint.Color;

import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.stage.Stage;
import javafx.stage.Modality;

import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.time.ZonedDateTime;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.MissingResourceException;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.time.Instant;
import java.time.temporal.WeekFields;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * JavaFX application main logic class.
 * 
 * Technical details
 * JavaFX 13
 * Java version 18.0.2
 * 
 */
public class App extends Application {    
    private DatabaseHelper _databaseHelper;
    
    private User _authenticatedUser;
    
    // These are class-wide variables because they are referenced by multiple functions
    // In order to update the table they have to be global, not local function variables
    private TableView<Customer> _customerTable;
    private TableView<Appointment> _appointmentTable;
    
    // for loading human language translations (i18n)
    private ResourceBundle _resourceBundle;

    // Calculate date values once
    private ArrayList<String> _currentYearWeekList, _currentYearMonthList;
    private WeekHelper [] _listOfWeeks;    
    
    /**
     * Class constructor
     */
    public App() {
        
    }
    
    /**
     * Show window for creating or updating a customer row.
     * Includes JavaFX form elements and form validation.
     * 
     * @return void
     */
    private void _createOrUpdateCustomerRecordWindow(int id) {
        // load data from database, customer table, and associated tables (country and first-level-divisions)
        var countryTableRows = this._databaseHelper.getCountryTable();
        var firstDivisionTableRows = this._databaseHelper.getAllRowsFromFirstLevelDivisionTable();
        var customerTableRows = this._databaseHelper.getAllRowsFromCustomerTable();        
        
        Customer customerInstance = null;
        
        for (int i = 0; i < customerTableRows.size(); i++) {
            if(customerTableRows.get(i).getId() == id) {
                customerInstance = customerTableRows.get(i);
            }
        }

        var newWindow = new Stage();
        newWindow.initModality(Modality.APPLICATION_MODAL);
        var titleLabel = new Label("");
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 14.0));
        
        if(customerInstance == null) { // create a new row
            newWindow.setTitle(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.windowTitle"));
            titleLabel.setText(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.titleLabel"));
        } else {
            newWindow.setTitle(this._resourceBundle.getString("guiScreen.customerTable.updateRow.windowTitle"));
            titleLabel.setText(this._resourceBundle.getString("guiScreen.customerTable.updateRow.titleLabel"));
        }

        var rootContainer = new VBox();
        
        var bodyContainer = new GridPane();
        bodyContainer.setPadding(new Insets(30)); // top, right, bottom, left
        bodyContainer.setHgap(10.0);
        bodyContainer.setVgap(10.0);
        
        var titleContainer = new HBox();     
        titleContainer.setPadding(new Insets(30, 0, 0, 30)); // top, right, bottom, left

        titleContainer.getChildren().add(titleLabel);
        rootContainer.getChildren().add(titleContainer);
        
        var customerIDLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.customerIDLabel"));
        var customerIDTextField = new TextField();

        customerIDTextField.setDisable(true);
        
        bodyContainer.add(customerIDLabel, 0, 0); // column=0, row=0
        bodyContainer.add(customerIDTextField, 1, 0);
        
        var nameLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.nameLabel"));
        var nameTextField = new TextField();
        
        bodyContainer.add(nameLabel, 0, 1); //column=0, row=1
        bodyContainer.add(nameTextField, 1, 1);

        var addressLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.addressLabel"));
        var addressTextField = new TextField();
        
        bodyContainer.add(addressLabel, 0, 2); //column=0, row=2
        bodyContainer.add(addressTextField, 1, 2); 

        var postalCodeLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.postalCodeLabel"));
        var postalCodeTextField = new TextField();
        
        bodyContainer.add(postalCodeLabel, 0, 3); //column=0, row=3
        bodyContainer.add(postalCodeTextField, 1, 3);

        var phoneNumberLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.phoneNumberLabel"));
        var phoneNumberTextField = new TextField();
        
        bodyContainer.add(phoneNumberLabel, 0, 4); //column=0, row=4
        bodyContainer.add(phoneNumberTextField, 1, 4);
        
        var countryArrayList = new ArrayList<String>();

        // lambda expression
        countryTableRows.forEach(row -> countryArrayList.add(row.getCountry()));
        
        ObservableList<String> countryComboBoxOptions = FXCollections.observableArrayList(countryArrayList);
        
        
        var countryLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.countryLabel"));
        ComboBox countryComboBox = new ComboBox(countryComboBoxOptions);
        countryComboBox.setPrefWidth(200.0);
        
        bodyContainer.add(countryLabel, 0, 5); //column=0, row=5
        bodyContainer.add(countryComboBox, 1, 5);

        var divisionLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.divisionLabel"));
        ComboBox divisionComboBox = new ComboBox(null);
        divisionComboBox.setPrefWidth(200.0);

        bodyContainer.add(divisionLabel, 0, 6); //column=0, row=6
        bodyContainer.add(divisionComboBox, 1, 6);        

        // Country ComboBox event listener, used to update the second ComboBox (First-level division)
        countryComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                var selectedCountryID = -1;
                
                // search through country rows for matching ComboBox selection
                for(int i=0; i < countryTableRows.size(); i++) {                    
                    // look for matching Country object index
                    if(countryTableRows.get(i).getCountry().matches(countryComboBox.getValue().toString())) {
                        // store the selected country row id
                        selectedCountryID = countryTableRows.get(i).getId();
                        var firstLevelDivisionArrayList = new ArrayList<String>();
                        
                        for(int k = 0; k < firstDivisionTableRows.size(); k++) {
                            
                            // update the first-level division ComboBox options, only listing the appropriate ones from the selected country
                            if(firstDivisionTableRows.get(k).getCountryID() == selectedCountryID) {
                                firstLevelDivisionArrayList.add(firstDivisionTableRows.get(k).getDivision());
                            }
                        }
                        // update all available items
                        divisionComboBox.setItems(FXCollections.observableArrayList(firstLevelDivisionArrayList));                      
                                               
                        // clear any existing selection
                        divisionComboBox.setValue(null);
                    }
                }
            }
        });
        
        // update form element values if necessary
        if (customerInstance == null) { // create new row
           customerIDTextField.setText(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.autoGeneratedLabel"));
        } else { // update existing row
            customerIDTextField.setText("" + customerInstance.getId());
            nameTextField.setText(customerInstance.getCustomerName());
            addressTextField.setText(customerInstance.getAddress());
            postalCodeTextField.setText(customerInstance.getPostalCode());
            phoneNumberTextField.setText(customerInstance.getPhoneNumber());
            countryComboBox.setValue(customerInstance.getCountryName());
            
            // trigger / first an ActionEvent to update the first-level division ComboBox listener, update the listed items
            countryComboBox.fireEvent(new ActionEvent());
            divisionComboBox.setValue(customerInstance.getDivisionName());
        }
        
        var buttonContainer = new HBox();
        buttonContainer.setSpacing(10.0);
        buttonContainer.setAlignment(Pos.TOP_RIGHT);
        buttonContainer.setPadding(new Insets(20.0, 0, 0, 0)); // top, right, bottom, left
        
        var saveButton = new Button(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.saveButton"));
        var cancelButton = new Button(this._resourceBundle.getString("guiScreen.customerTable.createNewRow.cancelButton"));
        buttonContainer.getChildren().addAll(saveButton, cancelButton);
        
        // 'Save' button pressed
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                
                // form error checking
                
                if(nameTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.customerTable.createNewRow.formError.nameRequired"));
                    alert.show();
                    return;
                }
                if(addressTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.customerTable.createNewRow.formError.addressRequired"));
                    alert.show();
                    return;
                }
                if(postalCodeTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.customerTable.createNewRow.formError.postalCodeRequired"));
                    alert.show();
                    return;
                }
                if(phoneNumberTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.customerTable.createNewRow.formError.phoneNumberRequired"));
                    alert.show();
                    return;
                }
                
                // country and first-level division ComboBoxes must be populated
                if (divisionComboBox.getSelectionModel().getSelectedItem() == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.customerTable.createNewRow.formError.divisionRequired"));
                    alert.show();
                    return;
                }
                
                if (id < 0) { // create a new row
                    var newCustomer = new Customer();
                    
                    newCustomer.setCustomerName(nameTextField.getText());
                    newCustomer.setAddress(addressTextField.getText());
                    newCustomer.setPostalCode(postalCodeTextField.getText());
                    newCustomer.setPhoneNumber(phoneNumberTextField.getText());
                    
                    // Set both Created_By and Last_Updated_By to the same username
                    newCustomer.setCreatedBy(_authenticatedUser.getUsername()); 
                    newCustomer.setLastUpdateBy(_authenticatedUser.getUsername());
                    
                    // We want the ID of the first-level division, search table rows for matching ComboBox selection String
                    for (int i = 0; i < firstDivisionTableRows.size(); i++) {
                        if(firstDivisionTableRows.get(i).getDivision().matches(divisionComboBox.getSelectionModel().getSelectedItem().toString())) {
                            newCustomer.setDivisionID(firstDivisionTableRows.get(i).getId());
                        }
                    }
                    
                    // verify that country and first-level division are set
                    if(newCustomer.getDivisionID() > 0) { // attempt to create the new row
                        // run SQL
                        _databaseHelper.createCustomerRow(newCustomer);
                        
                        // update the customer table
                        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromCustomerTable());
                        _customerTable.setItems(customerTableRows);                        
                        
                        // close the form window
                        newWindow.close();
                    }
                } else { // update an existing row
                    var existingCustomer = new Customer();
                    
                    existingCustomer.setCustomerName(nameTextField.getText());
                    existingCustomer.setAddress(addressTextField.getText());
                    existingCustomer.setPostalCode(postalCodeTextField.getText());
                    existingCustomer.setPhoneNumber(phoneNumberTextField.getText());
                    
                    existingCustomer.setLastUpdateBy(_authenticatedUser.getUsername()); 
                    
                    // We want the ID of the first-level division, search table rows for matching ComboBox selection String
                    for (int i = 0; i < firstDivisionTableRows.size(); i++) {
                        if(firstDivisionTableRows.get(i).getDivision().matches(divisionComboBox.getSelectionModel().getSelectedItem().toString())) {
                            existingCustomer.setDivisionID(firstDivisionTableRows.get(i).getId());
                        }
                    }
                    
                    // verify that country and first-level division are set
                    if(existingCustomer.getDivisionID() > 0) { // attempt to create the new row
                        // run SQL
                        _databaseHelper.updateCustomerRow(id, existingCustomer);
                        
                        // update the customer table
                        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromCustomerTable());
                        _customerTable.setItems(customerTableRows);                        
                        
                        // close the form window
                        newWindow.close();
                    }                    
                }
            }
        });
        
        // 'Cancel' button pressed
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                newWindow.close();
            }
        });
        
        bodyContainer.add(buttonContainer, 1, 7);
        
        rootContainer.getChildren().add(bodyContainer);        
        
        newWindow.setScene(new Scene(rootContainer));
        newWindow.show();
    }

    /**
     * Show window for creating or updating appointment rows.
     * Includes JavaFX form elements and form validation.
     * 
     * @return void
     */    
    private void _createOrUpdateAppointmentRecordWindow(int id) {
        Appointment appointmentInstance = null;
        
        if (id > 0) { // load data from database, appointment table, and associated tables (contacts and customers)
            appointmentInstance = this._databaseHelper.getAppointmentRowByID(id);

            // Change the Start and End DT from UTC to local timezone
            this._changeAppointmentListDateToLocalTimezone(appointmentInstance);            
        }

        
        var contactTableRows = this._databaseHelper.getAllRowsFromContactTable();
        var customerTableRows = this._databaseHelper.getAllRowsFromCustomerTable();
        
        var newWindow = new Stage();
        newWindow.initModality(Modality.APPLICATION_MODAL);
        var headerLabel = new Label("");
        var headerLabelFont = headerLabel.getFont();
        headerLabel.setFont (headerLabelFont.font (headerLabelFont.getFamily(), FontWeight.BOLD, 14.0));
        
        if(appointmentInstance == null) { // create a new row
            newWindow.setTitle(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.windowTitle"));
            headerLabel.setText(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.titleLabel"));
        } else {
            newWindow.setTitle(this._resourceBundle.getString("guiScreen.appointmentTable.updateRow.windowTitle"));
            headerLabel.setText(this._resourceBundle.getString("guiScreen.appointmentTable.updateRow.titleLabel"));
        }
        
        var rootContainer = new VBox();
        
        var bodyContainer = new GridPane();
        bodyContainer.setPadding(new Insets(30)); // top, right, bottom, left
        bodyContainer.setHgap(10.0);
        bodyContainer.setVgap(10.0);
        
        var headerContainer = new HBox();     
        headerContainer.setPadding(new Insets(30, 0, 0, 30)); // top, right, bottom, left
        
        headerContainer.getChildren().add(headerLabel);
        rootContainer.getChildren().add(headerContainer);

        var appointmentIDLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.appointmentIDLabel"));
        appointmentIDLabel.setPrefWidth(140.0);
        var appointmentIDTextField = new TextField();
        appointmentIDTextField.setDisable(true);

        bodyContainer.add(appointmentIDLabel, 0, 0); // column=0, row=0
        bodyContainer.add(appointmentIDTextField, 1, 0);
        
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.title"));
        var titleTextField = new TextField();
        
        bodyContainer.add(titleLabel, 0, 1); //column=0, row=1
        bodyContainer.add(titleTextField, 1, 1);

        var descriptionLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.description"));
        var descriptionTextField = new TextField();
        
        bodyContainer.add(descriptionLabel, 0, 2); //column=0, row=2
        bodyContainer.add(descriptionTextField, 1, 2);

        var locationLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.location"));
        var locationTextField = new TextField();
        
        bodyContainer.add(locationLabel, 0, 3); //column=0, row=3
        bodyContainer.add(locationTextField, 1, 3);

        var typeLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.type"));
        var typeTextField = new TextField();
        
        bodyContainer.add(typeLabel, 0, 4); //column=0, row=4
        bodyContainer.add(typeTextField, 1, 4);
        
        var dateLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.date"));
        DatePicker datePicker = new DatePicker();
        datePicker.setPrefWidth(215.0);
        
        bodyContainer.add(dateLabel, 0, 5);
        bodyContainer.add(datePicker, 1, 5); // column=1, row=5
        
        var startTimeLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.startTime"));

        ObservableList<String> hourOptions = 
        FXCollections.observableArrayList(
            "00","01","02","03","04","05","06","07","08","09","10",
                "11","12","13","14","15","16","17","18","19","20",
                "21","22","23"
        );
        
        ObservableList<String> minuteOptions = 
        FXCollections.observableArrayList(
                "00","15","30","45"
        );        

        ComboBox startTimeHourComboBox = new ComboBox(hourOptions);
        startTimeHourComboBox.setPrefWidth(90.0);
        startTimeHourComboBox.setValue("08");
        
        ComboBox startTimeMinuteComboBox = new ComboBox(minuteOptions);
        startTimeMinuteComboBox.setPrefWidth(90.0);
        startTimeMinuteComboBox.setValue("00");
        
        var startTimeContainer = new HBox();
        var startTimeHourMinuteLabel = new Label(":");
        startTimeContainer.setSpacing(15.0);
        startTimeContainer.getChildren().add(startTimeHourComboBox);
        startTimeContainer.getChildren().add(startTimeHourMinuteLabel);
        startTimeContainer.getChildren().add(startTimeMinuteComboBox);
                
        bodyContainer.add(startTimeLabel, 0, 6); // column=0, row=6
        bodyContainer.add(startTimeContainer, 1, 6);

        
        var endTimeLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.endTime"));

        ComboBox endTimeHourComboBox = new ComboBox(hourOptions);
        endTimeHourComboBox.setPrefWidth(90.0);
        endTimeHourComboBox.setValue("09");
        
        ComboBox endTimeMinuteComboBox = new ComboBox(minuteOptions);
        endTimeMinuteComboBox.setPrefWidth(90.0);
        endTimeMinuteComboBox.setValue("00");
        
        var endTimeContainer = new HBox();
        var endTimeHourMinuteLabel = new Label(":");
        endTimeContainer.setSpacing(15.0);
        endTimeContainer.getChildren().add(endTimeHourComboBox);
        endTimeContainer.getChildren().add(endTimeHourMinuteLabel);
        endTimeContainer.getChildren().add(endTimeMinuteComboBox);        
        
        bodyContainer.add(endTimeLabel, 0, 7); // column=0, row=7
        bodyContainer.add(endTimeContainer, 1, 7);

        
        var contactArrayList = new ArrayList<String>();
        
        // lamba expression
        contactTableRows.forEach(row -> contactArrayList.add(row.getName()));
        
        ObservableList<String> contactComboBoxOptions = FXCollections.observableArrayList(contactArrayList);
        
        var contactLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.contact"));
        var contactComboBox = new ComboBox(contactComboBoxOptions);
        contactComboBox.setPrefWidth(210.0);

        bodyContainer.add(contactLabel, 0, 8); // column=0, row=8
        bodyContainer.add(contactComboBox, 1, 8);        

        
        var customerArrayList = new ArrayList<String>();
        for(int i=0; i < customerTableRows.size(); i++) {
            customerArrayList.add(customerTableRows.get(i).getCustomerName());
        }
        ObservableList<String> customerComboBoxOptions = FXCollections.observableArrayList(customerArrayList);        
        
        var customerLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.customer"));
        var customerComboBox = new ComboBox(customerComboBoxOptions);
        customerComboBox.setPrefWidth(210.0);
        
        bodyContainer.add(customerLabel, 0, 9); // column=0, row=9
        bodyContainer.add(customerComboBox, 1, 9);
                
        // update form element values if necessary
        if (appointmentInstance == null) { // create new row
           appointmentIDTextField.setText(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.autoGeneratedLabel"));
        } else { // update existing row
            appointmentIDTextField.setText("" + appointmentInstance.getId());
            titleTextField.setText(appointmentInstance.getTitle());
            descriptionTextField.setText(appointmentInstance.getDescription());
            locationTextField.setText(appointmentInstance.getLocation());
            typeTextField.setText(appointmentInstance.getType());

            datePicker.setValue(appointmentInstance.getAppointmentStart().toLocalDate());
            
            startTimeHourComboBox.setValue(appointmentInstance.getAppointmentStart().toLocalTime().getHour());
            
            // small bug, ComboBox option is "00" not "0", causes the ComboBox GUI to show "0"
            if(appointmentInstance.getAppointmentStart().toLocalTime().getMinute() == 0) {
                startTimeMinuteComboBox.setValue("00");                
            } else {
                startTimeMinuteComboBox.setValue(appointmentInstance.getAppointmentStart().toLocalTime().getMinute());                
            }
                       
            endTimeHourComboBox.setValue(appointmentInstance.getAppointmentEnd().toLocalTime().getHour());
            
            // small bug, ComboBox option is "00" not "0", causes the ComboBox GUI to show "0"
            if(appointmentInstance.getAppointmentEnd().toLocalTime().getMinute() == 0) {
                endTimeMinuteComboBox.setValue("00");
            } else {
                endTimeMinuteComboBox.setValue(appointmentInstance.getAppointmentEnd().toLocalTime().getMinute());
            }            
            
            contactComboBox.setValue(appointmentInstance.getContactName());
            customerComboBox.setValue(appointmentInstance.getCustomerName());            
        }
        
        // add the buttons 'Save' and 'Cancel'
        var buttonContainer = new HBox();
        buttonContainer.setSpacing(10.0);
        buttonContainer.setAlignment(Pos.TOP_RIGHT);
        buttonContainer.setPadding(new Insets(20.0, 0, 0, 0)); // top, right, bottom, left
        
        var saveButton = new Button(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.saveButton"));
        var cancelButton = new Button(this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.cancelButton"));
        buttonContainer.getChildren().addAll(saveButton, cancelButton);
        
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {

                // do form error checking
                if(titleTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.titleFieldRequired"));
                    alert.show();
                    return;
                }
                if(descriptionTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.descriptionFieldRequired"));
                    alert.show();
                    return;
                }
                if(locationTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.locationFieldRequired"));
                    alert.show();
                    return;
                }
                if(typeTextField.getText().length() < 1) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.typeFieldRequired"));
                    alert.show();
                    return;
                }
                if(datePicker.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.dateFieldRequired"));
                    alert.show();
                    return;
                }                
                if(customerComboBox.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.customerFieldRequired"));
                    alert.show();
                    return;              
                }
                if(contactComboBox.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.contactFieldRequired"));
                    alert.show();
                    return;              
                }

                int startHourValue = Integer.parseInt(startTimeHourComboBox.getValue().toString());
                int startMinuteValue = Integer.parseInt(startTimeMinuteComboBox.getValue().toString());
                var localStartDT = LocalDateTime.of(datePicker.getValue(), LocalTime.of(startHourValue, startMinuteValue));                
                
                int endHourValue = Integer.parseInt(endTimeHourComboBox.getValue().toString());
                int endMinuteValue = Integer.parseInt(endTimeMinuteComboBox.getValue().toString());
                var localEndDT = LocalDateTime.of(datePicker.getValue(), LocalTime.of(endHourValue, endMinuteValue));                
                
                var businessHourStartDT = LocalDateTime.of(datePicker.getValue(), LocalTime.of(8, 0));
                var businessHourEndDT = LocalDateTime.of(datePicker.getValue(), LocalTime.of(22, 0));

                // convert time to local timezone and seconds since epoch for easy comparison
                TimeZone localTZ = TimeZone.getTimeZone(TimeZone.getDefault().getID());
                var localOffsetSeconds = localTZ.getOffset(Calendar.ZONE_OFFSET) / 1000; // in milliseconds (Divide by 1000 converts to seconds) 
                
                var localStartEpochSeconds = localStartDT.toEpochSecond(ZoneOffset.ofTotalSeconds(localOffsetSeconds));
                var localEndEpochSeconds = localEndDT.toEpochSecond(ZoneOffset.ofTotalSeconds(localOffsetSeconds));
                
                // convert time to "America/New_York" (EST) timezone and seconds since epoch for easy comparison
                TimeZone estTZ = TimeZone.getTimeZone("America/New_York");                
                var estOffsetSeconds = estTZ.getOffset(Calendar.ZONE_OFFSET) / 1000; // in milliseconds (Divide by 1000 converts to seconds)                
                
                var businessHourStartEpochSeconds = businessHourStartDT.toEpochSecond(ZoneOffset.ofTotalSeconds(estOffsetSeconds));
                var businessHourEndEpochSeconds = businessHourEndDT.toEpochSecond(ZoneOffset.ofTotalSeconds(estOffsetSeconds));

                
                // Check if start or end time are bad
                if(localStartEpochSeconds > localEndEpochSeconds) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.startTimeAfterEndTime"));
                    alert.show();
                    return;
                }
                if(localStartEpochSeconds == localEndEpochSeconds) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.startTimeAfterEndTime"));
                    alert.show();
                    return;                    
                }                
                
                // Check if time is between available business hours
                if(localStartEpochSeconds < businessHourStartEpochSeconds) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.startTimeBeforeBusinessHours"));
                    alert.show();                    
                    return;                    
                }
                if(localEndEpochSeconds > businessHourEndEpochSeconds) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.endTimeAfterBusinessHours"));
                    alert.show();                    
                    return;         
                }
                
                // Convert from local timezone to UTC                          
                var utcStartDT = LocalDateTime.ofEpochSecond(
                        localStartDT.toInstant(ZonedDateTime.now().getOffset()).toEpochMilli() / 1000,
                        0,
                        ZoneOffset.UTC
                );                

                // Convert from local timezone to UTC                            
                var utcEndDT = LocalDateTime.ofEpochSecond(
                        localEndDT.toInstant(ZonedDateTime.now().getOffset()).toEpochMilli() / 1000,
                        0,
                        ZoneOffset.UTC
                );
                
                // Search the appointment table for conflicting times
                var conflictingRows = _databaseHelper.getAppointmentRowsByStartAndEnd(utcStartDT.toLocalDate(), utcEndDT.toLocalDate());
                
                // we only know that these rows are on the same date, now check the time
                for (int i = 0; i < conflictingRows.size(); i++) {
                    var row = conflictingRows.get(i);
                    
                    // check if the new event ends after the existing one starts, and starts before the existing event ends.
                    if(
                        id != row.getId() &&
                        utcEndDT.isAfter(row.getAppointmentStart()) &&
                        utcStartDT.isBefore(row.getAppointmentEnd()) &&
                        row.getCustomerName().matches(customerComboBox.getValue().toString())
                        ) {
                        _changeAppointmentListDateToLocalTimezone(row);                        
                        
                        // This error message is multi-line and has sprintf() formatting ("%s")
                        String errorMessage = _resourceBundle.getString("guiScreen.appointmentTable.createNewRow.formError.conflictingTime");
                        
                        var s1 = String.format("%s %s", row.getAppointmentStart().toLocalDate().toString(), row.getAppointmentStart().toLocalTime().toString());
                        var s2 = String.format("%s %s", row.getAppointmentEnd().toLocalDate().toString(), row.getAppointmentEnd().toLocalTime().toString());                        
                        var s3 = String.format(errorMessage, s1, s2);                        
                        
                        Alert alert = new Alert(Alert.AlertType.ERROR, s3);
                        alert.show();                    
                        return;                        
                    }
                }
                
                var newAppointment = new Appointment();
                newAppointment.setTitle(titleTextField.getText());
                newAppointment.setDescription(descriptionTextField.getText());
                newAppointment.setLocation(locationTextField.getText());
                newAppointment.setType(typeTextField.getText());
        
                // Search the customer rows for the matching row id
                for (int i = 0; i < customerTableRows.size(); i++) {
                    if(customerTableRows.get(i).getCustomerName().matches(customerComboBox.getValue().toString())) {
                        newAppointment.setCustomerID(customerTableRows.get(i).getId());
                    }
                }

                // Search the customer rows for the matching row id
                for (int i = 0; i < contactTableRows.size(); i++) {
                    if(contactTableRows.get(i).getName().matches(contactComboBox.getValue().toString())) {
                        newAppointment.setContactID(contactTableRows.get(i).getID());
                    }
                }
                
                newAppointment.setAppointmentStart(utcStartDT);                
                newAppointment.setAppointmentEnd(utcEndDT);
                
                newAppointment.setUserID(_authenticatedUser.getId());
                newAppointment.setLastUpdateBy(_authenticatedUser.getLastUpdatedBy());
                
                if(id < 0) { // create a new row
                    newAppointment.setCreatedBy(_authenticatedUser.getUsername());                    
                    
                    // run SQL command
                    _databaseHelper.createAppointmentRow(newAppointment);
                        
                    // update the customer table
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromAppointmentTable());
                    
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    _appointmentTable.setItems(appointmentTableRows);                        

                    // close the form window
                    newWindow.close();
                } else { // update an existing row
                    // run SQL command
                    _databaseHelper.updateAppointmentRow(id, newAppointment);
                        
                    // update the customer table
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromAppointmentTable());
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    _appointmentTable.setItems(appointmentTableRows);                        

                    // close the form window
                    newWindow.close();                    
                }
            }
        });
        
        // 'Cancel' button pressed
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                newWindow.close();
            }
        });
        
        bodyContainer.add(buttonContainer, 1, 10);
        
        
        rootContainer.getChildren().add(bodyContainer);                
        
        newWindow.setScene(new Scene(rootContainer));
        newWindow.show();        
    }
    
    /**
     * Contains the logic for the login or authentication JavaFX window.
     * Searches the database for a matching username and password.
     * 
     * @param stage 
     */
    private void _showUserLoginScreen(Stage stage) {        
        this._resourceBundle = null;
        
        // For the English / French translation (i18n)
        // If there is an error loading the .property file, exit
        try {
            // use existing Operating System settings for french or english
            Locale.setDefault(new Locale(Locale.getDefault().getLanguage(), Locale.getDefault().getCountry()));
            this._resourceBundle = ResourceBundle.getBundle("ApplicationStrings");
        } catch (MissingResourceException e) {
            System.out.println("Error loading Property File (Used for internationalization / translation)!");
            e.printStackTrace();
            System.exit(1);
        }
        
        // Change the window title
        stage.setTitle(this._resourceBundle.getString("guiScreen.general.applicationWindowTitle"));
        
        var rootContainer = new VBox();
        rootContainer.setPrefSize(1200.0, 700.0);
        rootContainer.setAlignment(Pos.CENTER);
        
        var bodyContainer = new GridPane();
        bodyContainer.setAlignment(Pos.CENTER);
        bodyContainer.setHgap(10.0);
        bodyContainer.setVgap(15.0);               
        
        
        var titleContainer = new HBox();
        titleContainer.setAlignment(Pos.CENTER);
        
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.general.applicationLabel"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 24.0));
        
        titleContainer.setPadding(new Insets(0, 0, 20, 0)); // top, right, bottom, left
        titleContainer.getChildren().add(titleLabel);

        rootContainer.getChildren().add(titleContainer);
        
        var formFont = Font.font(titleLabel.getFont().getFamily(), FontWeight.NORMAL, 18.0);
        
        var humanLanguageLabel = new Label(this._resourceBundle.getString("guiScreen.userLogin.humanLanguageTitle"));
        humanLanguageLabel.setFont(formFont);
        
        var radioButtonContainer = new HBox();
        radioButtonContainer.setSpacing(15.0);
        var radioButtonGroup = new ToggleGroup();
        
        var englishRadioButton = new RadioButton(this._resourceBundle.getString("guiScreen.userLogin.humanLanguageEnglish"));
        englishRadioButton.setFont(formFont);
        englishRadioButton.setToggleGroup(radioButtonGroup);
        
        var frenchRadioButton = new RadioButton(this._resourceBundle.getString("guiScreen.userLogin.humanLanguageFrench"));
        frenchRadioButton.setFont(formFont);
        frenchRadioButton.setToggleGroup(radioButtonGroup);
        
        // set the radio button based on the Operating System Locale
        if(Locale.getDefault().getLanguage().matches("fr")) {
            frenchRadioButton.setSelected(true);
        } else {
            englishRadioButton.setSelected(true);
        }
        
        radioButtonContainer.getChildren().addAll(englishRadioButton, frenchRadioButton);
        
        bodyContainer.add(humanLanguageLabel, 0, 0); // column=0, row=0
        bodyContainer.add(radioButtonContainer, 1, 0);
        
        
        var usernameTextField = new TextField();
        var usernameLabel = new Label(this._resourceBundle.getString("guiScreen.userLogin.usernameLabel"));
        usernameLabel.setFont(formFont);
        usernameTextField.setPrefWidth(400.0);
        usernameTextField.setFont(formFont);
        
        bodyContainer.add(usernameLabel, 0, 1); // column=0, row=1
        bodyContainer.add(usernameTextField, 1, 1);
        
        var passwordTextField = new PasswordField();
        passwordTextField.setFont(formFont);
        var passwordLabel = new Label(this._resourceBundle.getString("guiScreen.userLogin.passwordLabel"));
        passwordLabel.setFont(formFont);
        
        bodyContainer.add(passwordLabel, 0, 2); // column=0, row=2
        bodyContainer.add(passwordTextField, 1, 2);
        
        var timezoneLabel = new Label(this._resourceBundle.getString("guiScreen.userLogin.timezoneLabel"));
        timezoneLabel.setFont(formFont);
        String timezoneString = TimeZone.getDefault().getID() + " UTC " + ZonedDateTime.now().getOffset().toString();
        var userTimezoneTextField = new TextField();
        userTimezoneTextField.setText(timezoneString);
        userTimezoneTextField.setFont(formFont);
        userTimezoneTextField.setDisable(true);
        
        bodyContainer.add(timezoneLabel, 0, 3); // column=0, row=3
        bodyContainer.add(userTimezoneTextField, 1, 3);
        
        var formErrorContainer = new HBox();
        var formErrorLabel = new Label();
        formErrorLabel.setFont(formFont);
        formErrorLabel.setTextFill(Color.RED);
        formErrorContainer.getChildren().add(formErrorLabel);
        formErrorContainer.setVisible(false);
        
        bodyContainer.add(formErrorContainer, 1, 4);
        
        
        var buttonsContainer = new HBox();
        buttonsContainer.setAlignment(Pos.BOTTOM_RIGHT);
        var submitButton = new Button(this._resourceBundle.getString("guiScreen.userLogin.submitButton"));
        submitButton.setFont(formFont);
        buttonsContainer.getChildren().add(submitButton);

        bodyContainer.add(buttonsContainer, 1, 5);

        // Create a listener for the RadioButtonGroup, appears after other form elements declarations because it needs to reference them
        radioButtonGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle oldToggle, Toggle newToggle) {
                RadioButton current = (RadioButton) newToggle;
                
                // english RadioButton selected
                if(current.getText().matches(_resourceBundle.getString("guiScreen.userLogin.humanLanguageEnglish"))) {
                    Locale.setDefault(new Locale("en", "US"));
                    _resourceBundle = ResourceBundle.getBundle("ApplicationStrings");
                } else { // french selected
                    Locale.setDefault(new Locale("fr", "CA"));
                    _resourceBundle = ResourceBundle.getBundle("ApplicationStrings");
                }

                // update form element text
                stage.setTitle(_resourceBundle.getString("guiScreen.general.applicationWindowTitle"));
                titleLabel.setText(_resourceBundle.getString("guiScreen.general.applicationLabel"));
                usernameLabel.setText(_resourceBundle.getString("guiScreen.userLogin.usernameLabel"));
                passwordLabel.setText(_resourceBundle.getString("guiScreen.userLogin.passwordLabel"));
                timezoneLabel.setText(_resourceBundle.getString("guiScreen.userLogin.timezoneLabel"));
                submitButton.setText(_resourceBundle.getString("guiScreen.userLogin.submitButton"));
                humanLanguageLabel.setText(_resourceBundle.getString("guiScreen.userLogin.humanLanguageTitle"));
                englishRadioButton.setText(_resourceBundle.getString("guiScreen.userLogin.humanLanguageEnglish"));
                frenchRadioButton.setText(_resourceBundle.getString("guiScreen.userLogin.humanLanguageFrench"));
                formErrorContainer.setVisible(false);
            }
        });
        
        var userTableRows = this._databaseHelper.getAllRowsFromUserTable();
        
        // 'Submit' button pressed
        submitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                // clear past error messages
                formErrorLabel.setText("");
                formErrorContainer.setVisible(false);
                
                FileWriter fileWriter = null;
                BufferedWriter bufferedWriter = null;
                PrintWriter streamWriter = null;
                
                try {
                    fileWriter = new FileWriter("./login_activity.txt", true);
                    bufferedWriter = new BufferedWriter(fileWriter);
                    streamWriter = new PrintWriter(bufferedWriter);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

                // For debugging path to file
                // System.out.println("Wrote file to: " + System.getProperty("user.dir") + "/login_activity.txt");                
                
                // form fields validation
                if(usernameTextField.getText().length() == 0) {
                    formErrorLabel.setText(_resourceBundle.getString("guiScreen.userLogin.formError.usernameRequired"));

                    // log failed attempt to file
                    if (streamWriter != null && bufferedWriter != null) {
                        String s = String.format("User attempted to login at '%s'. Attempt failed. Reason: 'Username is required'", 
                                Instant.now().toString());
                        streamWriter.println(s);
                        streamWriter.flush();
                        
                        try {
                            bufferedWriter.close();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                    
                    formErrorContainer.setVisible(true);
                    return;
                }
                if(passwordTextField.getText().length() == 0) {
                    formErrorLabel.setText(_resourceBundle.getString("guiScreen.userLogin.formError.passwordRequired"));
                    formErrorContainer.setVisible(true);
                    
                    // log failed attempt to file
                    if (streamWriter != null && bufferedWriter != null) {
                        String s = String.format("User attempted to login at '%s'. Attempt failed. Reason: 'Password is required'", 
                                Instant.now().toString());
                        streamWriter.println(s);
                        streamWriter.flush();
                        
                        try {
                            bufferedWriter.close();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                    
                    return;
                }    

                // search the user table for a matching username and password
                User u = _databaseHelper.getUserRowByUsername(usernameTextField.getText());
                if (u == null) { // username not found
                    formErrorLabel.setText(_resourceBundle.getString("guiScreen.userLogin.formError.usernameNotFound"));
                    formErrorContainer.setVisible(true);
                    
                    // log failed attempt to file
                    if (streamWriter != null && bufferedWriter != null) {
                        String s = String.format("User: '%s' attempted to login at '%s'. Attempt failed. Reason: 'Username not found'", 
                                usernameTextField.getText(), Instant.now().toString());
                        streamWriter.println(s);
                        streamWriter.flush();
                        
                        try {
                            bufferedWriter.close();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                } else { // username found, check password
                    if(u.getPassword().matches(passwordTextField.getText())) {
                        // log failed attempt to file
                        if (streamWriter != null && bufferedWriter != null) {
                            String s = String.format("User: '%s' successfully logged in at '%s'.",
                                    usernameTextField.getText(), Instant.now().toString());
                            streamWriter.println(s);
                            streamWriter.flush();

                            try {
                                bufferedWriter.close();
                            } catch (IOException ioe) {
                                ioe.printStackTrace();
                            }
                        }

                        _authenticatedUser = u;
                        _showMainScreen(stage);   
                    } else { // invalid password
                        formErrorLabel.setText(_resourceBundle.getString("guiScreen.userLogin.formError.passwordInvalid"));
                        formErrorContainer.setVisible(true);
                        
                        // log failed attempt to file
                        if (streamWriter != null && bufferedWriter != null) {
                            String s = String.format("User: '%s' attempted to login at '%s'. Attempt failed. Reason: 'Invalid password'",
                                    usernameTextField.getText(), Instant.now().toString());
                            streamWriter.println(s);
                            streamWriter.flush();

                            try {
                                bufferedWriter.close();
                            } catch (IOException ioe) {
                                ioe.printStackTrace();
                            }
                        }                        
                    }
                }
            }
        });                
        
        rootContainer.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(rootContainer);
        stage.setScene(scene);        
    }    

    /**
     * Change Appointment LocalDateTime variables from UTC to Local Computer timezone
     * Uses a lambda expression to loop through the appointment row instances.
     * @param rows 
     */
    private void _changeAppointmentListDateToLocalTimezone(ObservableList<Appointment> rows) {    
        // Change row times from UTC to local timezone        
        rows.forEach(row -> this._changeAppointmentListDateToLocalTimezone(row));
    }

    /**
     * Change an Appointment instance LocalDateTime variables (Columns 'Start' and 'End') from UTC to Local Computer timezone.
     * @param row 
     */
    private void _changeAppointmentListDateToLocalTimezone(Appointment row) {
        // Careful when converting Instant type milliseconds to seconds, divide by 1000
        var instantStartDate = row.getAppointmentStart().toInstant(ZoneOffset.UTC);            
        var startDT = LocalDateTime.ofEpochSecond(instantStartDate.toEpochMilli() / 1000, 0, ZonedDateTime.now().getOffset());

        var instantEndDate = row.getAppointmentEnd().toInstant(ZoneOffset.UTC);
        var endDT = LocalDateTime.ofEpochSecond(instantEndDate.toEpochMilli() / 1000, 0, ZonedDateTime.now().getOffset());

        row.setAppointmentStart(startDT);
        row.setAppointmentEnd(endDT);
    }
    
    /**
     * Contains the logic for displaying the Appointment database table, with elements to make it more user-friendly.
     * Includes buttons for creating, updating, or deleting existing appointments.
     * Translates UTC database DATETIME columns to the local timezone.
     * 
     * @param stage JavaFX window variable
     */
    private void _showAppointmentsTable(Stage stage) {
        VBox container = new VBox();
        container.setPrefSize(1200.0, 700.0);

        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);

        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));
        
        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentTable.label"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add RadioButton row
        var radioButtonContainer = new GridPane();
        radioButtonContainer.setPadding(new Insets(10.0, 0, 20.0, 0)); // top, right, bottom, and left
        var horizontalSeparator = new Separator();
        
        var gridPaneColumnConstraints1 = new ColumnConstraints();
        gridPaneColumnConstraints1.setPercentWidth(45);

        var gridPaneColumnConstraints2 = new ColumnConstraints();
        gridPaneColumnConstraints2.setPercentWidth(35);
        
        var gridPaneColumnConstraints3 = new ColumnConstraints();
        gridPaneColumnConstraints3.setPercentWidth(20);
        
        radioButtonContainer.getColumnConstraints().addAll(gridPaneColumnConstraints1, gridPaneColumnConstraints2, gridPaneColumnConstraints3);
        
        var radioButtonGroup = new ToggleGroup();        
        var viewByWeekRadioButton = new RadioButton(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.viewWeek"));
        viewByWeekRadioButton.setDisable(true);
        viewByWeekRadioButton.setToggleGroup(radioButtonGroup);
        
        var viewByMonthRadioButton = new RadioButton(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.viewMonth"));
        viewByMonthRadioButton.setDisable(true);
        viewByMonthRadioButton.setToggleGroup(radioButtonGroup);
        
        var viewAllRadioButton = new RadioButton(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.viewAll"));
        viewAllRadioButton.setToggleGroup(radioButtonGroup);
        viewAllRadioButton.setSelected(true);
        
        var weekComboBox = new ComboBox(this._getWeekList());
        var monthComboBox = new ComboBox(this._getMonthList());
        
        var column01Container = new HBox();
        column01Container.setSpacing(15.0);
        column01Container.getChildren().addAll(viewByWeekRadioButton, weekComboBox);

        var column02Container = new HBox();
        column02Container.setSpacing(15.0);
        column02Container.getChildren().addAll(viewByMonthRadioButton, monthComboBox);
        
        radioButtonContainer.add(column01Container, 0, 0);
        radioButtonContainer.add(column02Container, 1, 0);
        radioButtonContainer.add(viewAllRadioButton, 2, 0);
        
        bodyContainer.getChildren().add(radioButtonContainer);
        bodyContainer.getChildren().add(horizontalSeparator);

        // 'View All' RadioButton listener
        radioButtonGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle oldToggle, Toggle newToggle) {
                RadioButton current = (RadioButton) newToggle;
                
                // reset the ComboBox elements
                if(current.getText().matches(_resourceBundle.getString("guiScreen.appointmentTable.radioButtons.viewAll"))) {
                    weekComboBox.getSelectionModel().clearSelection();
                    monthComboBox.getSelectionModel().clearSelection();
                    
                    // update the appointment table
                    ZoneId localTimezone = ZoneId.of(TimeZone.getDefault().getID());
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromAppointmentTable());

                    // Change row times from UTC to local timezone
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    
                    _appointmentTable.setItems(appointmentTableRows);                    
                }
            }
        });
        
        // 'View Week' ComboBox event listener, used to select RadioButton
        weekComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if(weekComboBox.getSelectionModel().getSelectedItem() != null) {
                    viewByWeekRadioButton.setSelected(true);
                    monthComboBox.getSelectionModel().clearSelection();
                    int selectedIndex = weekComboBox.getSelectionModel().selectedIndexProperty().get();
                    
                    // update the appointment table
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(
                            _databaseHelper.getAppointmentRowsByStartAndEnd(
                                _listOfWeeks[selectedIndex].getStart(), _listOfWeeks[selectedIndex].getEnd()
                            )
                    );
                    
                    // Change row times from UTC to local timezone
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    
                    _appointmentTable.setItems(appointmentTableRows);
                }
            }
        });

        // 'View Month' ComboBox event listener, used to select RadioButton
        monthComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if(monthComboBox.getSelectionModel().getSelectedItem() != null) {                
                    viewByMonthRadioButton.setSelected(true);
                    weekComboBox.getSelectionModel().clearSelection();

                            
                    int selectedIndex = monthComboBox.getSelectionModel().selectedIndexProperty().get();
                    var startOfMonth = LocalDate.of(LocalDate.now().getYear(), Month.of(selectedIndex + 1), 1);
                    var endOfMonth = startOfMonth.with(TemporalAdjusters.lastDayOfMonth());
                    
                    // update the appointment table
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(
                            _databaseHelper.getAppointmentRowsByStartAndEnd(
                                startOfMonth, endOfMonth
                            )
                    );
                    
                    // Change row times from UTC to local timezone
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    
                    _appointmentTable.setItems(appointmentTableRows);                    
                }
            }
        });
        
        // add buttons row
        var buttonContainer = new HBox();
        buttonContainer.setSpacing(10.0);
        buttonContainer.setPadding(new Insets(10.0, 0, 20.0, 0)); // top, right, bottom, and left

        buttonContainer.setAlignment(Pos.TOP_LEFT);
        var createButton = new Button(this._resourceBundle.getString("guiScreen.appointmentTable.buttons.create"));
        var updateButton = new Button(this._resourceBundle.getString("guiScreen.appointmentTable.buttons.update"));
        updateButton.setDisable(true);
        var deleteButton = new Button(this._resourceBundle.getString("guiScreen.appointmentTable.buttons.delete"));
        deleteButton.setDisable(true);
        
        buttonContainer.getChildren().addAll(createButton, updateButton, deleteButton);
        bodyContainer.getChildren().add(buttonContainer);

        // add the table
        this._appointmentTable = new TableView<Appointment>();                
        ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromAppointmentTable());
        
        // Change row times from UTC to local timezone
        this._changeAppointmentListDateToLocalTimezone(appointmentTableRows);
        
        this._appointmentTable.setItems(appointmentTableRows);
        
        // add the table column titles
        var appointmentColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.id"));
        appointmentColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 3 ); // set percentage of width
        appointmentColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("id")
        );

        var appointmentTitleColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.title"));
        appointmentTitleColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 7 ); // set percentage of width
        appointmentTitleColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("title")
        );

        var appointmentDescriptionColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.description"));
        appointmentDescriptionColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentDescriptionColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("description")
        );

        var appointmentLocationColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.location"));
        appointmentLocationColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentLocationColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("location")
        );

        var appointmentTypeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.type"));
        appointmentTypeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentTypeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("type")
        );

        var appointmentStartTimeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.start"));
        appointmentStartTimeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        appointmentStartTimeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("appointmentStart")
        );
        
        var appointmentEndTimeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.end"));
        appointmentEndTimeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        appointmentEndTimeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("appointmentEnd")
        );

        var appointmentCustomerColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.customer"));
        appointmentCustomerColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentCustomerColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("customerName")
        );

        var appointmentUserColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.user"));
        appointmentUserColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentUserColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("userName")
        );
        
        var appointmentContactColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.contact"));
        appointmentContactColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentContactColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("contactName")
        );        
        
        this._appointmentTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this._appointmentTable.getColumns().addAll(
                appointmentColumn,
                appointmentTitleColumn,
                appointmentDescriptionColumn,
                appointmentLocationColumn,
                appointmentTypeColumn,
                appointmentStartTimeColumn,
                appointmentEndTimeColumn,
                appointmentCustomerColumn,
                appointmentUserColumn,
                appointmentContactColumn
        );

        // add a listener to the TableView row selection
        this._appointmentTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Appointment>() {
            @Override public void changed(ObservableValue<? extends Appointment> ov, Appointment oldSelection, Appointment newSelection) {                
                // disable the 'Update' and 'Delete' button unless a row is selected. (Cannot update or delete an non-existing row)
                
                if (newSelection != null) {
                    updateButton.setDisable(false);
                    deleteButton.setDisable(false);
                } else {
                    updateButton.setDisable(true);
                    deleteButton.setDisable(true);
                }
            }
        });        
        
        // button event listeners

        // 'Create' button pressed
        createButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _createOrUpdateAppointmentRecordWindow(-1);
            }
        });

        // 'Update' button pressed
        updateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _createOrUpdateAppointmentRecordWindow(_appointmentTable.getSelectionModel().getSelectedItem().getId());
            }
        });
        
        // 'Delete' button pressed
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                int appointmentID = _appointmentTable.getSelectionModel().getSelectedItem().getId();
                
                // show a confirmation dialog window
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, _resourceBundle.getString("guiScreen.appointmentTable.deleteRow.questionLabel"));

                var noButton = new ButtonType(_resourceBundle.getString("guiScreen.appointmentTable.deleteRow.noButton"));
                var yesButton = new ButtonType(_resourceBundle.getString("guiScreen.appointmentTable.deleteRow.yesButton"));

                alert.getButtonTypes().setAll(noButton, yesButton);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == yesButton) {
                    // remove the row
                    _databaseHelper.deleteAppointmentByID(appointmentID);

                    // update the appointment table
                    ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromAppointmentTable());
                    
                    // Change row times from UTC to local timezone
                    _changeAppointmentListDateToLocalTimezone(appointmentTableRows);
                    
                    _appointmentTable.setItems(appointmentTableRows);
                } else if (result.get() == noButton) {
                    // do nothing
                }
            }
        });
        
        bodyContainer.getChildren().add(this._appointmentTable);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);
    }
    
    /**
     * One of the required Report windows. Contains JavaFX GUI elements.
     * Allows Appointment rows to be shown by referencing a foreign table column, 'apointment.Contact_ID'
     * 
     * @param stage JavaFX window variable
     */
    private void _showAppointmentsByContact(Stage stage) {
        var container = new VBox();
        container.setPrefSize(1200.0, 700.0);
        
        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);
        
        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));

        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentsByContact.titleLabel"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add ComboBox row
        var comboBoxContainer = new GridPane();
        comboBoxContainer.setPadding(new Insets(10.0, 0, 10.0, 0)); // top, right, bottom, and left
        var horizontalSeparator = new Separator();
        
        var contactList = FXCollections.observableArrayList(this._databaseHelper.getContactList());
        
        var contactLabel = new Label(this._resourceBundle.getString("guiScreen.appointmentsByContact.contactLabel"));
        var contactComboBox = new ComboBox(contactList);
        
        var column01Container = new HBox();
        column01Container.setSpacing(15.0);
        column01Container.getChildren().addAll(contactLabel, contactComboBox);
        
        comboBoxContainer.add(column01Container, 0, 0);
        
        bodyContainer.getChildren().add(comboBoxContainer);
        bodyContainer.getChildren().add(horizontalSeparator);
        
        // add the table
        var tableContainer = new VBox();
        tableContainer.setPadding(new Insets(10, 0, 0, 0)); // top, right, bottom, and left
        
        var appointmentTable = new TableView<Appointment>();        
        ObservableList<Appointment> appointmentTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromAppointmentTable());
        appointmentTable.setItems(appointmentTableRows);
        
        // add the table column titles
        var appointmentColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.id"));
        appointmentColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 3 ); // set percentage of width
        appointmentColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("id")
        );

        var appointmentTitleColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.title"));
        appointmentTitleColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 7 ); // set percentage of width
        appointmentTitleColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("title")
        );

        var appointmentDescriptionColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.description"));
        appointmentDescriptionColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentDescriptionColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("description")
        );

        var appointmentLocationColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.location"));
        appointmentLocationColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentLocationColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("location")
        );

        var appointmentTypeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.type"));
        appointmentTypeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentTypeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("type")
        );

        var appointmentStartTimeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.start"));
        appointmentStartTimeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        appointmentStartTimeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("appointmentStart")
        );
        
        var appointmentEndTimeColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.end"));
        appointmentEndTimeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        appointmentEndTimeColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("appointmentEnd")
        );

        var appointmentCustomerColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.customer"));
        appointmentCustomerColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentCustomerColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("customerName")
        );

        var appointmentUserColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.user"));
        appointmentUserColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentUserColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("userName")
        );
        
        var appointmentContactColumn = new TableColumn(this._resourceBundle.getString("guiScreen.appointmentTable.tableColumns.contact"));
        appointmentContactColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        appointmentContactColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Appointment,String>("contactName")
        );        
        
        appointmentTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        appointmentTable.getColumns().addAll(
                appointmentColumn,
                appointmentTitleColumn,
                appointmentDescriptionColumn,
                appointmentLocationColumn,
                appointmentTypeColumn,
                appointmentStartTimeColumn,
                appointmentEndTimeColumn,
                appointmentCustomerColumn,
                appointmentUserColumn,
                appointmentContactColumn
        );
                
        contactComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                var observableList = FXCollections.observableArrayList(
                        _databaseHelper.getAppointmentRowsByContactName(contactComboBox.getValue().toString())
                );
                
                appointmentTable.setItems(observableList);
            }
        });                
                
        tableContainer.getChildren().add(appointmentTable);
        bodyContainer.getChildren().add(tableContainer);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);                
    }

    /**
     * One of the required Report windows. Contains JavaFX GUI elements.
     * Allows Appointment rows to be shown by referencing a foreign table column, 'customer.Customer_ID' -> 'country.Country_ID'
     * 
     * @param stage JavaFX window variable
     */    
    private void _showCustomersByCountryScreen(Stage stage) {
        var container = new VBox();
        container.setPrefSize(1200.0, 700.0);
        
        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);
        
        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));

        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.customersByCountryScreen.titleLabel"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add ComboBox row
        var comboBoxContainer = new GridPane();
        comboBoxContainer.setPadding(new Insets(10.0, 0, 10.0, 0)); // top, right, bottom, and left
        var horizontalSeparator = new Separator();
                
        var countryOptions = FXCollections.observableArrayList(
                this._databaseHelper.getCountryList()
        );
        
        var customerCountryLabel = new Label(this._resourceBundle.getString("guiScreen.customersByCountryScreen.countryLabel"));
        var customerCountryComboBox = new ComboBox(countryOptions);
        
        var column01Container = new HBox();
        column01Container.setSpacing(15.0);
        column01Container.getChildren().addAll(customerCountryLabel, customerCountryComboBox);
        
        comboBoxContainer.add(column01Container, 0, 0);
        
        bodyContainer.getChildren().add(comboBoxContainer);
        bodyContainer.getChildren().add(horizontalSeparator);        
                
        // add the table
        var tableContainer = new VBox();
        tableContainer.setPadding(new Insets(10, 0, 0, 0)); // top, right, bottom, and left
        
        var customerTable = new TableView<Customer>();        
        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromCustomerTable());
        customerTable.setItems(customerTableRows);
        
        // add the table column titles
        var customerIDTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.id"));
        customerIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 5 ); // set percentage of width
        customerIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("id")
        );

        var customerNameTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.name"));
        customerNameTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 18 ); // set percentage of width
        customerNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("customerName")
        );
        
        var customerAddressTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.address"));
        customerAddressTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 17 ); // set percentage of width
        customerAddressTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("address")
        );
        
        var customerPostalCodeTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.postalCode"));
        customerPostalCodeTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        customerPostalCodeTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("postalCode")
        ); 

        var customerPhoneNumberTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.phoneNumber"));
        customerPhoneNumberTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerPhoneNumberTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("phoneNumber")
        );

        var customerDivisionTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.division"));
        customerDivisionTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // set percentage of width
        customerDivisionTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("divisionName")
        );
        
        var customerCountryTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.country"));
        customerCountryTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerCountryTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("countryName")
        );
        
        customerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        customerTable.getColumns().addAll(
                customerIDTableColumn,
                customerNameTableColumn,
                customerAddressTableColumn,
                customerPostalCodeTableColumn,
                customerPhoneNumberTableColumn,
                customerDivisionTableColumn,
                customerCountryTableColumn
        );


        customerCountryComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                
                var observableList = FXCollections.observableArrayList(
                        _databaseHelper.getCustomerRowsByCountry(customerCountryComboBox.getValue().toString())
                );
                
                customerTable.setItems(observableList);
            }
        });
        
        tableContainer.getChildren().add(customerTable);
        bodyContainer.getChildren().add(tableContainer);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);  
    }

    /**
     * One of the required Report windows. Contains JavaFX GUI elements.
     * Allows Appointment rows to be shown / sorted by 'appointment.Start' and 'appointment.End' column.
     * 
     * @param stage JavaFX window variable
     */    
    private void _showCustomersByMonthScreen(Stage stage) {
        var container = new VBox();
        container.setPrefSize(1200.0, 700.0);
        
        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);
        
        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));

        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.customersByMonthScreen.titleLabel"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add ComboBox row
        var comboBoxContainer = new GridPane();
        comboBoxContainer.setPadding(new Insets(10.0, 0, 10.0, 0)); // top, right, bottom, and left
        var horizontalSeparator = new Separator();
                
        var monthOptions = this._getMonthList();
        
        var appointmentMonthLabel = new Label(this._resourceBundle.getString("guiScreen.customersByMonthScreen.appointmentMonthLabel"));
        var appointmentMonthComboBox = new ComboBox(monthOptions);
        
        var column01Container = new HBox();
        column01Container.setSpacing(15.0);
        column01Container.getChildren().addAll(appointmentMonthLabel, appointmentMonthComboBox);
        
        comboBoxContainer.add(column01Container, 0, 0);
        
        bodyContainer.getChildren().add(comboBoxContainer);
        bodyContainer.getChildren().add(horizontalSeparator);        
                
        // add the table
        var tableContainer = new VBox();
        tableContainer.setPadding(new Insets(10, 0, 0, 0)); // top, right, bottom, and left
        
        var customerTable = new TableView<Customer>();        
        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromCustomerTable());
        customerTable.setItems(customerTableRows);
        
        // add the table column titles
        var customerIDTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.id"));
        customerIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 5 ); // set percentage of width
        customerIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("id")
        );

        var customerNameTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.name"));
        customerNameTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 18 ); // set percentage of width
        customerNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("customerName")
        );
        
        var customerAddressTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.address"));
        customerAddressTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 17 ); // set percentage of width
        customerAddressTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("address")
        );
        
        var customerPostalCodeTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.postalCode"));
        customerPostalCodeTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        customerPostalCodeTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("postalCode")
        ); 

        var customerPhoneNumberTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.phoneNumber"));
        customerPhoneNumberTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerPhoneNumberTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("phoneNumber")
        );

        var customerDivisionTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.division"));
        customerDivisionTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // set percentage of width
        customerDivisionTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("divisionName")
        );
        
        var customerCountryTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.country"));
        customerCountryTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerCountryTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("countryName")
        );
        
        customerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        customerTable.getColumns().addAll(
                customerIDTableColumn,
                customerNameTableColumn,
                customerAddressTableColumn,
                customerPostalCodeTableColumn,
                customerPhoneNumberTableColumn,
                customerDivisionTableColumn,
                customerCountryTableColumn
        );


        appointmentMonthComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                int selectedIndex = appointmentMonthComboBox.getSelectionModel().selectedIndexProperty().get();
                var startOfMonth = LocalDate.of(LocalDate.now().getYear(), Month.of(selectedIndex + 1), 1);
                var endOfMonth = startOfMonth.with(TemporalAdjusters.lastDayOfMonth());
                
                var observableList = FXCollections.observableArrayList(
                        _databaseHelper.getCustomerRowsByAppointmentMonth(startOfMonth, endOfMonth)
                );
                
                customerTable.setItems(observableList);
            }
        });
        
        tableContainer.getChildren().add(customerTable);
        bodyContainer.getChildren().add(tableContainer);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);    
    }

    /**
     * One of the required Report windows. Contains JavaFX GUI elements.
     * Allows Appointment rows to be shown / sorted by the 'appointment.Type' column.
     * 
     * @param stage JavaFX window variable
     */    
    private void _showCustomersByTypeScreen(Stage stage) {
        var container = new VBox();
        container.setPrefSize(1200.0, 700.0);
        
        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);
        
        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));

        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.customersByTypeScreen.titleLabel"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add ComboBox row
        var comboBoxContainer = new GridPane();
        comboBoxContainer.setPadding(new Insets(10.0, 0, 10.0, 0)); // top, right, bottom, and left
        var horizontalSeparator = new Separator();
                
        var typeOptions = FXCollections.observableArrayList(this._databaseHelper.getDistinctTypeColumnsFromAppointmentTable());
        
        var appointmentTypeLabel = new Label(this._resourceBundle.getString("guiScreen.customersByTypeScreen.appointmentTypeLabel"));
        var appointmentTypeComboBox = new ComboBox(typeOptions);
        
        var column01Container = new HBox();
        column01Container.setSpacing(15.0);
        column01Container.getChildren().addAll(appointmentTypeLabel, appointmentTypeComboBox);
        
        comboBoxContainer.add(column01Container, 0, 0);
        
        bodyContainer.getChildren().add(comboBoxContainer);
        bodyContainer.getChildren().add(horizontalSeparator);        
                
        // add the table
        var tableContainer = new VBox();
        tableContainer.setPadding(new Insets(10, 0, 0, 0)); // top, right, bottom, and left
        
        var customerTable = new TableView<Customer>();
        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromCustomerTable());
        customerTable.setItems(customerTableRows);
        
        // add the table column titles
        var customerIDTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.id"));
        customerIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 5 ); // set percentage of width
        customerIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("id")
        );

        var customerNameTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.name"));
        customerNameTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 18 ); // set percentage of width
        customerNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("customerName")
        );
        
        var customerAddressTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.address"));
        customerAddressTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 17 ); // set percentage of width
        customerAddressTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("address")
        );
        
        var customerPostalCodeTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.postalCode"));
        customerPostalCodeTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        customerPostalCodeTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("postalCode")
        ); 

        var customerPhoneNumberTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.phoneNumber"));
        customerPhoneNumberTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerPhoneNumberTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("phoneNumber")
        );

        var customerDivisionTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.division"));
        customerDivisionTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // set percentage of width
        customerDivisionTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("divisionName")
        );
        
        var customerCountryTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.country"));
        customerCountryTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerCountryTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("countryName")
        );
        
        customerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        customerTable.getColumns().addAll(
                customerIDTableColumn,
                customerNameTableColumn,
                customerAddressTableColumn,
                customerPostalCodeTableColumn,
                customerPhoneNumberTableColumn,
                customerDivisionTableColumn,
                customerCountryTableColumn
        );


        appointmentTypeComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                
                var observableList = FXCollections.observableArrayList(
                        _databaseHelper.getCustomerRowsByAppointmentType(appointmentTypeComboBox.getValue().toString())
                );
                
                customerTable.setItems(observableList);
            }
        });
        
        tableContainer.getChildren().add(customerTable);
        bodyContainer.getChildren().add(tableContainer);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);
    }


    /**
     * Displays the customer database table in a user-friendly format.
     * 
     * @param stage JavaFX window variable
     */
    private void _showCustomersTable(Stage stage) {        
        var container = new VBox();
        container.setPrefSize(1200.0, 700.0);
        
        // add the window top menu
        var menuBar = _getWindowTopMenu(stage);
        container.getChildren().add(menuBar);
        
        // make a separate container to not affect the window top menu
        var bodyContainer = new VBox();
        bodyContainer.setPadding(new Insets(20.0));
        
        // add a text label
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.customerTable.label"));
        var titleLabelFont = titleLabel.getFont();
        titleLabel.setFont (titleLabelFont.font (titleLabelFont.getFamily(), FontWeight.BOLD, 16.0));
        titleLabel.setPadding(new Insets(0, 0, 10.0, 0)); // top, right, bottom, and left
        
        bodyContainer.getChildren().add(titleLabel);
        
        // add buttons row
        var buttonContainer = new HBox();
        buttonContainer.setSpacing(10.0);
        buttonContainer.setPadding(new Insets(0, 0, 20.0, 0)); // top, right, bottom, and left
        
        buttonContainer.setAlignment(Pos.TOP_LEFT);
        var createButton = new Button(this._resourceBundle.getString("guiScreen.customerTable.buttons.create"));
        var updateButton = new Button(this._resourceBundle.getString("guiScreen.customerTable.buttons.update"));
        updateButton.setDisable(true);
        var deleteButton = new Button(this._resourceBundle.getString("guiScreen.customerTable.buttons.delete"));
        deleteButton.setDisable(true);        
        
        buttonContainer.getChildren().addAll(createButton, updateButton, deleteButton);        
        bodyContainer.getChildren().add(buttonContainer);
        
        // add the table
        this._customerTable = new TableView<Customer>();
        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(this._databaseHelper.getAllRowsFromCustomerTable());
        this._customerTable.setItems(customerTableRows);
        
        // add the table column titles
        var customerIDTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.id"));
        customerIDTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 5 ); // set percentage of width
        customerIDTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("id")
        );

        var customerNameTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.name"));
        customerNameTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 18 ); // set percentage of width
        customerNameTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("customerName")
        );
        
        var customerAddressTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.address"));
        customerAddressTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 17 ); // set percentage of width
        customerAddressTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("address")
        );
        
        var customerPostalCodeTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.postalCode"));
        customerPostalCodeTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 10 ); // set percentage of width
        customerPostalCodeTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("postalCode")
        ); 

        var customerPhoneNumberTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.phoneNumber"));
        customerPhoneNumberTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerPhoneNumberTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("phoneNumber")
        );

        var customerDivisionTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.division"));
        customerDivisionTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 20 ); // set percentage of width
        customerDivisionTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("divisionName")
        );
        
        var customerCountryTableColumn = new TableColumn(this._resourceBundle.getString("guiScreen.customerTable.tableColumns.country"));
        customerCountryTableColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // set percentage of width
        customerCountryTableColumn.setCellValueFactory( // associate table column with class property
            new PropertyValueFactory <Customer,String>("countryName")
        );
        
        this._customerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this._customerTable.getColumns().addAll(
                customerIDTableColumn,
                customerNameTableColumn,
                customerAddressTableColumn,
                customerPostalCodeTableColumn,
                customerPhoneNumberTableColumn,
                customerDivisionTableColumn,
                customerCountryTableColumn
        );
        

        // add a listener to the TableView row selection
        this._customerTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Customer>() {
            @Override public void changed(ObservableValue<? extends Customer> ov, Customer oldSelection, Customer newSelection) {                
                // disable the 'Update' and 'Delete' button unless a row is selected. (Cannot update or delete an non-existing row)
                
                if (newSelection != null) {
                    updateButton.setDisable(false);
                    deleteButton.setDisable(false);
                } else {
                    updateButton.setDisable(true);
                    deleteButton.setDisable(true);
                }
            }
        });
        
        // button event listeners

        // 'Create' button pressed
        createButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                _createOrUpdateCustomerRecordWindow(-1);
            }
        });

        // 'Update' button pressed
        updateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {                
                _createOrUpdateCustomerRecordWindow(_customerTable.getSelectionModel().getSelectedItem().getId());
            }
        });
        
        // 'Delete' button pressed
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                int customerID = _customerTable.getSelectionModel().getSelectedItem().getId();
                
                // check database for associated rows from 'appointments' table, cannot delete if rows found
                int associatedRows = _databaseHelper.getAppointmentRowsByCustomerID(customerID);
                if(associatedRows > 0) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, 
                            _resourceBundle.getString("guiScreen.customerTable.deleteRow.formError.messageLine1") 
                            + "\n\n" + 
                            _resourceBundle.getString("guiScreen.customerTable.deleteRow.formError.messageLine2")
                    );
                    alert.setWidth(500);
                    alert.setTitle(_resourceBundle.getString("guiScreen.customerTable.deleteRow.formError.titleLabel"));
                    alert.show();
                } else { // no associated rows found

                    // show a confirmation dialog window
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, _resourceBundle.getString("guiScreen.customerTable.deleteRow.questionLabel"));

                    var noButton = new ButtonType(_resourceBundle.getString("guiScreen.customerTable.deleteRow.noButton"));
                    var yesButton = new ButtonType(_resourceBundle.getString("guiScreen.customerTable.deleteRow.yesButton"));

                    alert.getButtonTypes().setAll(noButton, yesButton);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == yesButton) {
                        // remove the row
                        _databaseHelper.deleteCustomerByID(customerID);

                        // update the customer table
                        ObservableList<Customer> customerTableRows = FXCollections.observableArrayList(_databaseHelper.getAllRowsFromCustomerTable());
                        _customerTable.setItems(customerTableRows);
                    } else if (result.get() == noButton) {
                        // do nothing
                    }        
                }
            }
        });        

        bodyContainer.getChildren().add(this._customerTable);        
        container.getChildren().add(bodyContainer);
        
        Scene scene = new Scene(container);
        stage.setScene(scene);
    }
    
    /**
     * Contains the logic for creating a JavaFX Window Menu, with the event listeners.
     * 
     * @param stage JavaFX window variable
     * @return 
     */
    private MenuBar _getWindowTopMenu(Stage stage) {        
        var fileMenu = new Menu(this._resourceBundle.getString("guiScreen.general.topMenu.fileMenu.label"));
        
        var logoutMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.fileMenu.logoutLabel"));
        
        var exitMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.fileMenu.exitLabel"));
        fileMenu.getItems().addAll(logoutMenuItem, exitMenuItem);
        
        // File MenuItem event listeners        
        logoutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {       
                _authenticatedUser = null;
                _showUserLoginScreen(stage);
            }
        });
        
        exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        
        var viewMenu = new Menu(this._resourceBundle.getString("guiScreen.general.topMenu.viewMenu.label"));
        var mainScreenMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.viewMenu.mainScreenLabel"));
        
        var reportsSubMenu = new Menu(this._resourceBundle.getString("guiScreen.general.topMenu.viewMenu.customerReportsLabel"));
        var contactScheduleSubMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.appointmentsByContact.titleLabel"));
        var customersByTypeSubMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.customersByTypeScreen.titleLabel"));
        var customersByMonthSubMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.customersByMonthScreen.titleLabel"));
        var customersByCountrySubMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.customersByCountryScreen.titleLabel"));
        
        reportsSubMenu.getItems().addAll(
                contactScheduleSubMenuItem,
                customersByTypeSubMenuItem,
                customersByMonthSubMenuItem,
                customersByCountrySubMenuItem
        );      
        
        var appointmentMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.viewMenu.appointmentsTableLabel"));
        var customersMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.viewMenu.customersTableLabel"));
        viewMenu.getItems().addAll(mainScreenMenuItem, appointmentMenuItem, customersMenuItem, reportsSubMenu);

        // Reports SubMenu
        contactScheduleSubMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showAppointmentsByContact(stage);
            }
        });
        
        customersByTypeSubMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showCustomersByTypeScreen(stage);
            }
        });

        customersByMonthSubMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showCustomersByMonthScreen(stage);
            }
        });

        customersByCountrySubMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showCustomersByCountryScreen(stage);
            }
        });
        
        // MenuItem event listeners
        mainScreenMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showMainScreen(stage);
            }
        });
        
        appointmentMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showAppointmentsTable(stage);
            }
        });

        customersMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                _showCustomersTable(stage);
            }
        });
        
        var helpMenu = new Menu(this._resourceBundle.getString("guiScreen.general.topMenu.helpMenu.label"));
        var aboutMenuItem = new MenuItem(this._resourceBundle.getString("guiScreen.general.topMenu.helpMenu.aboutLabel"));
        helpMenu.getItems().add(aboutMenuItem);

        // event listener
        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {                
                Alert alert = new Alert(Alert.AlertType.INFORMATION,
                        _resourceBundle.getString("guiScreen.general.topMenu.helpMenu.aboutScreen.descriptionLine1") 
                        + "\n" +
                        _resourceBundle.getString("guiScreen.general.topMenu.helpMenu.aboutScreen.descriptionLine2")
                        + "\n"
                );                
                alert.setWidth(500);
                alert.setTitle(_resourceBundle.getString("guiScreen.general.topMenu.helpMenu.aboutScreen.title"));
                alert.setHeaderText(_resourceBundle.getString("guiScreen.general.topMenu.helpMenu.aboutScreen.headerText"));
                alert.show();
            }
        });
        
        var menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, viewMenu, helpMenu);
        
        return menuBar;
    }
    
    /**
     * Helper function for sorting appointments by weeks in a year, 53 weeks are shown because some years are not fully captured by 52 weeks.
     * Creates 53 week strings for the current year.
     * 
     * @return ObservableList<String> Expected datatype for a ComboBox item list
     */
    private ObservableList<String> _getWeekList() {
        // calculate values once
        if(this._currentYearWeekList == null) {
            this._currentYearWeekList = new ArrayList<String>();

            // get first day of current week
            var firstDayOfCurrentWeek = WeekFields.of(Locale.getDefault()).getFirstDayOfWeek();       
            var startOfYear = LocalDate.now().with(TemporalAdjusters.firstDayOfYear());
            var firstDayOfWeekStartOfYear = startOfYear.with(TemporalAdjusters.previousOrSame(firstDayOfCurrentWeek));

            this._listOfWeeks = new WeekHelper[53];
            
            String weekLabel = this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.week.weekLabel");

            for(int i = 0; i < 53; i++) {
                var firstDayOfWeek = firstDayOfWeekStartOfYear.plusWeeks(i);
                var lastDayOfWeek = firstDayOfWeek.plusDays(6);

                this._listOfWeeks [i] = new WeekHelper(i + 1, firstDayOfWeek,lastDayOfWeek);
                this._currentYearWeekList.add(this._listOfWeeks[i].toString(weekLabel));
            }
        }
        
        return FXCollections.observableArrayList(this._currentYearWeekList);
    }

    /**
     * Helper function for sorting appointments by months in a year.
     * Creates 12 week strings for the current year.
     * 
     * @return ObservableList<String> Expected datatype for a ComboBox item list
     */    
    private ObservableList<String> _getMonthList() {
        if (this._currentYearMonthList == null) {
            this._currentYearMonthList = new ArrayList<String>();

            int year = LocalDate.now().getYear();

            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.january") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.february") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.march") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.april") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.may") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.june") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.july") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.august") + " " + year);            
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.september") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.october") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.november") + " " + year);
            this._currentYearMonthList.add(this._resourceBundle.getString("guiScreen.appointmentTable.radioButtons.month.december") + " " + year);
        }
        
        return FXCollections.observableArrayList(this._currentYearMonthList);
    }
    
    /**
     * Contains the logic for landing page, after a user authenticates / logins to the application.
     * 
     * @param stage JavaFX window variable
     */
    private void _showMainScreen(Stage stage) {        
        var menuBar = _getWindowTopMenu(stage);
                
        var container = new VBox();
        container.getChildren().add(menuBar);
        container.setPrefSize(1200.0, 700.0);
        
        var headerContainer = new VBox();
        headerContainer.setPadding(new Insets(20.0));
        
        var titleLabel = new Label(this._resourceBundle.getString("guiScreen.mainScreen.titleLabel"));
        var defaultFont = titleLabel.getFont();
        titleLabel.setFont(defaultFont.font (defaultFont.getFamily(), FontWeight.BOLD, 22.0));
        
        headerContainer.getChildren().add(titleLabel);
        container.getChildren().add(headerContainer);
        
        var informationTableContainer = new GridPane();
        informationTableContainer.setPadding(new Insets(20, 0, 20, 0)); // top, right, bottom, and left
        informationTableContainer.setHgap(20.0);
        informationTableContainer.setVgap(10.0);
        
        var userLabel = new Label(this._resourceBundle.getString("guiScreen.mainScreen.userLabel"));
        userLabel.setFont(defaultFont.font (defaultFont.getFamily(), FontWeight.NORMAL, 18.0));
        var userValueLabel = new Label();
        userValueLabel.setText(this._authenticatedUser.getUsername());
        userValueLabel.setFont(userLabel.getFont());
        
        informationTableContainer.add(userLabel, 0, 0);
        informationTableContainer.add(userValueLabel, 1, 0);
        
        var dateAndTimeLabel = new Label(this._resourceBundle.getString("guiScreen.mainScreen.currentDateAndTimeLabel"));
        dateAndTimeLabel.setFont(userLabel.getFont());
        var dateAndTimeValueLabel = new Label();
        
        String s1 = String.format("%s %s", LocalDate.now().toString(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        dateAndTimeValueLabel.setText(s1);
        dateAndTimeValueLabel.setFont(userLabel.getFont());
        
        informationTableContainer.add(dateAndTimeLabel, 0, 1);
        informationTableContainer.add(dateAndTimeValueLabel, 1, 1);

        String timezoneString = TimeZone.getDefault().getID() + " UTC " + ZonedDateTime.now().getOffset().toString();        
        var timezoneLabel = new Label(this._resourceBundle.getString("guiScreen.mainScreen.timezoneLabel"));
        timezoneLabel.setFont(userLabel.getFont());
        var timezoneValueLabel = new Label();
        timezoneValueLabel.setText(timezoneString);
        timezoneValueLabel.setFont(userLabel.getFont());

        informationTableContainer.add(timezoneLabel, 0, 2);
        informationTableContainer.add(timezoneValueLabel, 1, 2);
        
        headerContainer.getChildren().add(informationTableContainer);
        
        var horizontalSeparator = new Separator();
        headerContainer.getChildren().add(horizontalSeparator);
        
        var instructionsContainer = new VBox();
        instructionsContainer.setPadding(new Insets(10, 0, 0, 0)); // top, right, bottom, and left
        var instructionsLabel = new Label(this._resourceBundle.getString("guiScreen.mainScreen.instructionsLabel"));
        
        instructionsLabel.setFont(userLabel.getFont());
        instructionsContainer.getChildren().add(instructionsLabel);

        headerContainer.getChildren().add(instructionsContainer);
                
        var scene = new Scene(container);
        
        stage.setScene(scene);
        stage.show();
        
        // check for upcoming appointments
        var nowDT = LocalDateTime.now(ZoneOffset.UTC);
        var soonDT = nowDT.plusMinutes(15);        
        
        var rows = this._databaseHelper.getAppointmentRowsByStartAndEnd(nowDT, soonDT);
        
        if(rows.size() > 0) { // there is an appointment within the next 15 minutes            
            
            var message = "";
            
            for (int i = 0; i < rows.size(); i++) {
                var row = rows.get(i);
                
                _changeAppointmentListDateToLocalTimezone(row);
                
                var startS = String.format("%s %s", 
                        row.getAppointmentStart().toLocalDate().toString(), 
                        row.getAppointmentStart().toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
                );

                var endS = String.format("%s %s", 
                        row.getAppointmentEnd().toLocalDate().toString(), 
                        row.getAppointmentEnd().toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
                );                
                
                var s2 = this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.appointmentIDLabel");
                var s3 = this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.title");
                var s4 = this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.startTime");
                var s5 = this._resourceBundle.getString("guiScreen.appointmentTable.createNewRow.endTime");
                
                message += String.format("%s %d %s %s\n%s %s %s %s\n\n", s2, row.getId(), s3, row.getTitle(), s4, startS, s5, endS);                
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION, this._resourceBundle.getString("guiScreen.mainScreen.upcomingAppointment") + message);
            alert.setWidth(600);
            alert.setTitle(this._resourceBundle.getString("guiScreen.mainScreen.upcomingAppointmentTitle"));
            alert.show();
        } else { // no appointments within 15 minutes
            Alert alert = new Alert(Alert.AlertType.INFORMATION, this._resourceBundle.getString("guiScreen.mainScreen.noUpcomingAppointment"));
            alert.setTitle(this._resourceBundle.getString("guiScreen.mainScreen.upcomingAppointmentTitle"));
            alert.setWidth(600);
            alert.show();            
        }
        
        
    }
    
    /**
     * The entry point of JavaFX GUI application.
     * @param stage JavaFX window variable
     */
    @Override
    public void start(Stage stage) {        
        this._authenticatedUser = null;
        this._databaseHelper = new DatabaseHelper();        
        
        this._showUserLoginScreen(stage);

        stage.setTitle("WGU C195 - Software 02 - Performance Assessment");
        stage.show();        
    }

    /**
     * Entry point of a java application.
     * @param args Terminal / Console strings passed with invoking a java program.
     */
    public static void main(String[] args) {
        launch();
    }

}