/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Database model representing the 'countries' table columns.
 * @author user
 */
public class Country {
    private int _id;
    private String _country;
    private String _createdBy, _lastUpdatedBy;
    private LocalDate _createdOnLocalDate, _updatedOnLocalDate;
    private LocalTime _createdOnLocalTime, _updatedOnLocalTime;

    /**
     * Class constructor
     */
    public Country() {
    }

    /**
     * Getter function
     * @return integer, row id, 'countries.Country_ID'
     */
    public int getId() {
        return this._id;
    }

    /**
     * Getter function
     * @return String, country name, 'countries.Country'
     */
    public String getCountry() {
        return this._country;
    }

    /**
     * Getter function
     * @return String, 'countries.Created_By'
     */
    public String getCreatedBy() {
        return this._createdBy;
    }

    /**
     * Getter function
     * @return String, 'countries.Last_Updated_By'
     */    
    public String getLastUpdatedBy() {
        return this._lastUpdatedBy;
    }
    
    
    /**
     * Getter function
     * @return LocalDate, 'countries.Created_On', DATETIME column
     */    
    public LocalDate getCreatedOnLocalDate() {
        return this._createdOnLocalDate;
    }

    /**
     * Getter function
     * @return LocalTime, 'countries.Created_On', DATETIME column
     */        
    public LocalTime getCreatedOnLocalTime() {
        return this._createdOnLocalTime;
    }

    /**
     * Getter function
     * @return LocalDate, 'countries.Updated_On', DATETIME column
     */
    public LocalDate getUpdatedOnLocalDate() {
        return this._updatedOnLocalDate;
    }

    /**
     * Getter function
     * @return LocalTime, 'countries.Updated_On', DATETIME column
     */    
    public LocalTime getUpdatedOnLocalTime() {
        return this._updatedOnLocalTime;
    }
    
    /**
     * Setter function
     * @param id integer, row id
     */
    public void setId(int id) {
        this._id = id;
    }
    
    /**
     * Setter function
     * @param country String, country name
     */
    public void setCountry(String country) {
        this._country = country;
    }
    
    /**
     * Setter function
     * @param createdBy, String username of author / creator of row 
     */
    public void setCreatedBy(String createdBy) {
        this._createdBy = createdBy;
    }

    /**
     * Setter function
     * @param lastUpdateBy, String username of author / updater of row 
     */    
    public void setLastUpdateBy(String lastUpdateBy) {
        this._lastUpdatedBy = lastUpdateBy;
    } 
    
    /**
     * Setter function
     * @param createdOn, LocalDate, represents DATETIME column 
     */
    public void setCreatedOnLocalDate(LocalDate createdOn) {
        this._createdOnLocalDate = createdOn;
    }

    /**
     * Setter function
     * @param createdOn, LocalDate, represents DATETIME column 
     */    
    public void setCreatedOnLocalTime(LocalTime createdOn) {
        this._createdOnLocalTime = createdOn;
    }

    /**
     * Setter function
     * @param updatedOn, LocalDate, represents DATETIME column 
     */    
    public void setUpdatedOnLocalDate(LocalDate updatedOn) {
        this._updatedOnLocalDate = updatedOn;
    }

    /**
     * Setter function
     * @param updatedOn, LocalDate, represents DATETIME column 
     */    
    public void setUpdatedOnLocalTime(LocalTime updatedOn) {
        this._updatedOnLocalTime = updatedOn;
    }

}
