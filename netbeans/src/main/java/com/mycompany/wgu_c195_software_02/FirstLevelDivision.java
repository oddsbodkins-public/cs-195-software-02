/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.wgu_c195_software_02;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Database model representing the 'first_level_divisions' table columns.
 * @author user
 */
public class FirstLevelDivision {
    private int _id, _countryID;
    private String _division;
    private String _createdBy, _lastUpdatedBy;
    private LocalDate _createdOnLocalDate, _updatedOnLocalDate;
    private LocalTime _createdOnLocalTime, _updatedOnLocalTime;    

    public FirstLevelDivision() {
        this._id = -1;
        this._countryID = -1;
    }

    /**
     * Getter function
     * @return integer, row id, 'first_level_divisions.Division_ID'
     */
    public int getId() {
        return this._id;
    }
    
    
    /**
     * Getter function
     * @return String, 'first_level_divisions.Division'
     */
    public String getDivision() {
        return this._division;
    }
    
    /**
     * Getter function
     * @return int, 'first_level_divisions.Country_ID'
     */
    public int getCountryID() {
        return this._countryID;
    }
    
    /**
     * Getter function 
     * @return String, 'first_level_divisions.Created_By'
     */    
    public String getCreatedBy() {
        return this._createdBy;
    }


    /**
     * Getter function 
     * @return String, 'first_level_divisions.Updated_By'
     */    
    public String getLastUpdatedBy() {
        return this._lastUpdatedBy;
    }
    
    /**
     * Getter function 
     * @return LocalDate, 'first_level_divisions.Created_On', represents a DATETIME column
     */
    public LocalDate getCreatedOnLocalDate() {
        return this._createdOnLocalDate;
    }

    /**
     * Getter function 
     * @return LocalTime, 'first_level_divisions.Created_On', represents a DATETIME column
     */    
    public LocalTime getCreatedOnLocalTime() {
        return this._createdOnLocalTime;
    }

    /**
     * Getter function 
     * @return LocalDate, 'first_level_divisions.Updated_On', represents a DATETIME column
     */
    public LocalDate getUpdatedOnLocalDate() {
        return this._updatedOnLocalDate;
    }

    /**
     * Getter function 
     * @return LocalTime, 'first_level_divisions.Updated_On', represents a DATETIME column
     */    
    public LocalTime getUpdatedOnLocalTime() {
        return this._updatedOnLocalTime;
    }
    
    /**
     * Setter function
     * @param id integer, row id
     */
    public void setId(int id) {
        this._id = id;
    }
    
    /**
     * Setter function
     * @param division, division name 
     */
    public void setDivision(String division) {
        this._division = division;
    }
    
    /**
     * Setter function
     * @param id integer, country id
     */
    public void setCountryID(int id) {
        this._countryID = id;
    }
    
    /**
     * Setter function
     * @param createdBy String 
     */    
    public void setCreatedBy(String createdBy) {
        this._createdBy = createdBy;
    }
    
    /**
     * Setter function
     * @param lastUpdateBy String 
     */    
    public void setLastUpdateBy(String lastUpdateBy) {
        this._lastUpdatedBy = lastUpdateBy;
    } 
    
    /**
     * Setter function
     * @param createdOn LocalDate
     */
    public void setCreatedOnLocalDate(LocalDate createdOn) {
        this._createdOnLocalDate = createdOn;
    }

    /**
     * Setter function
     * @param createdOn LocalTime
     */    
    public void setCreatedOnLocalTime(LocalTime createdOn) {
        this._createdOnLocalTime = createdOn;
    }

    
    /**
     * Setter function
     * @param updatedOn LocalDate
     */    
    public void setUpdatedOnLocalDate(LocalDate updatedOn) {
        this._updatedOnLocalDate = updatedOn;
    }

    /**
     * Setter function
     * @param updatedOn LocalTime
     */    
    public void setUpdatedOnLocalTime(LocalTime updatedOn) {
        this._updatedOnLocalTime = updatedOn;
    }    
    
}
