Class: WGU C195 Software 02
Assignment: Company Scheduling Application, Performance Assessment
Date: 07-Nov-2022

------------

Technical information:

NetBeans Project with Mavin build tool
JavaFX 13 NetBeans module
Product Version: Apache NetBeans IDE 15
Java: 18.0.2; OpenJDK 64-Bit Server VM 18.0.2+9
Runtime: OpenJDK Runtime Environment 18.0.2+9
System: Debian 12, Linux version 5.19.0-1-amd64 running on amd64; UTF-8; en_US (nb)

-------------

Directions on how to run:

1. Open NetBeans
2. On the top window menu, go to "File->Import Project->From Zip"
3. Click the "Zip File" 'Browse' button.
4. Locate the Uploaded zip file.
5. On the top window menu, go to "Run->Build Project" and then "Run->Run Project"

--------------

Additional Report, Part A3F

I included a report for viewing customers with scheduled appointments by their country. 
Open the screen from the top window menu and make a selection in the drop-down / ComboBox element.

-------------

MySQL Connector driver

Version: mysql-connector-j-8.0.31.jar

I added this a dependency in my Netbeans project (Maven).

( Maven repository: https://mvnrepository.com/artifact/com.mysql/mysql-connector-j/8.0.31 )
( https://maven.apache.org/what-is-maven.html )

JavaDoc
-------------

The generated JavaDoc files are inside the zip file at this location:

[Zip file Root]/wgu_c195_software_02/target/site/apidocs/index.html




Other useful web links
--------------

Java Class documentation

https://docs.oracle.com/javase/8/javafx/api/allclasses-noframe.html

JavaFX
------
https://docs.oracle.com/javafx/2/get_started/jfxpub-get_started.htm
https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm#CHDGHCDG
https://docs.oracle.com/javafx/2/ui_controls/table-view.htm

MariaDB JDBC Connector
---------
https://mariadb.com/kb/en/about-mariadb-connector-j/
https://mariadb.org/connector-java/all-releases/
