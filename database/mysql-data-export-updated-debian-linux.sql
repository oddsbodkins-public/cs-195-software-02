CREATE DATABASE client_schedule;

CREATE USER 'wgu'@'localhost' IDENTIFIED BY 'Software 02 - C195';
GRANT ALL PRIVILEGES ON client_schedule.* TO 'wgu'@'localhost';
FLUSH PRIVILEGES;
quit -- Quit and login as newly created user to avoid using root

-- Linux command: 
-- mysql -u wgu -p # This is prompt for the password

USE client_schedule; -- Select newly created database

CREATE TABLE `countries` (
  `Country_ID` int NOT NULL AUTO_INCREMENT,
  `Country` varchar(50) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Created_By` varchar(50) DEFAULT NULL,
  `Last_Update` timestamp NULL DEFAULT NULL,
  `Last_Updated_By` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Country_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'U.S','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script'),(2,'UK','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script'),(3,'Canada','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `users` (
  `User_ID` int NOT NULL AUTO_INCREMENT,
  `User_Name` varchar(50) DEFAULT NULL,
  `Password` text,
  `Create_Date` datetime DEFAULT NULL,
  `Created_By` varchar(50) DEFAULT NULL,
  `Last_Update` timestamp NULL DEFAULT NULL,
  `Last_Updated_By` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`User_ID`),
  UNIQUE KEY `User_Name_UNIQUE` (`User_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test','test','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script'),(2,'admin','admin','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `contacts` (
  `Contact_ID` int NOT NULL AUTO_INCREMENT,
  `Contact_Name` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Contact_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Anika Costa','acoasta@company.com'),(2,'Daniel Garcia','dgarcia@company.com'),(3,'Li Lee','llee@company.com');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `first_level_divisions` (
  `Division_ID` int NOT NULL AUTO_INCREMENT,
  `Division` varchar(50) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Created_By` varchar(50) DEFAULT NULL,
  `Last_Update` timestamp NULL DEFAULT NULL,
  `Last_Updated_By` varchar(50) DEFAULT NULL,
  `Country_ID` int NOT NULL,
  PRIMARY KEY (`Division_ID`),
  KEY `fk_country_id_idx` (`Country_ID`),
  CONSTRAINT `fk_country_id` FOREIGN KEY (`Country_ID`) REFERENCES `countries` (`Country_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3979 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `first_level_divisions` WRITE;
/*!40000 ALTER TABLE `first_level_divisions` DISABLE KEYS */;
INSERT INTO `first_level_divisions` VALUES (1,'Alabama','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(2,'Arizona','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(3,'Arkansas','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(4,'California','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(5,'Colorado','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(6,'Connecticut','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(7,'Delaware','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(8,'District of Columbia','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(9,'Florida','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(10,'Georgia','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(11,'Idaho','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(12,'Illinois','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(13,'Indiana','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(14,'Iowa','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(15,'Kansas','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(16,'Kentucky','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(17,'Louisiana','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(18,'Maine','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(19,'Maryland','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(20,'Massachusetts','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(21,'Michigan','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(22,'Minnesota','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(23,'Mississippi','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(24,'Missouri','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(25,'Montana','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(26,'Nebraska','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(27,'Nevada','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(28,'New Hampshire','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(29,'New Jersey','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(30,'New Mexico','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(31,'New York','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(32,'North Carolina','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(33,'North Dakota','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(34,'Ohio','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(35,'Oklahoma','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(36,'Oregon','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(37,'Pennsylvania','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(38,'Rhode Island','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(39,'South Carolina','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(40,'South Dakota','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(41,'Tennessee','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(42,'Texas','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(43,'Utah','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(44,'Vermont','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(45,'Virginia','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(46,'Washington','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(47,'West Virginia','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(48,'Wisconsin','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(49,'Wyoming','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1),(52,'Hawaii','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(54,'Alaska','2021-09-10 13:14:09','script','2021-09-10 17:14:09','script',1),(60,'Northwest Territories','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(61,'Alberta','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(62,'British Columbia','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(63,'Manitoba','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(64,'New Brunswick','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(65,'Nova Scotia','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(66,'Prince Edward Island','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(67,'Ontario','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(68,'Québec','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(69,'Saskatchewan','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(70,'Nunavut','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(71,'Yukon','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(72,'Newfoundland and Labrador','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',3),(101,'England','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',2),(102,'Wales','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',2),(103,'Scotland','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',2),(104,'Northern Ireland','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',2);
/*!40000 ALTER TABLE `first_level_divisions` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `customers` (
  `Customer_ID` int NOT NULL AUTO_INCREMENT,
  `Customer_Name` varchar(50) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `Postal_Code` varchar(50) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Created_By` varchar(50) DEFAULT NULL,
  `Last_Update` timestamp NULL DEFAULT NULL,
  `Last_Updated_By` varchar(50) DEFAULT NULL,
  `Division_ID` int NOT NULL,
  PRIMARY KEY (`Customer_ID`),
  KEY `fk_division_id_idx` (`Division_ID`),
  CONSTRAINT `fk_division_id` FOREIGN KEY (`Division_ID`) REFERENCES `first_level_divisions` (`Division_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Daddy Warbucks','1919 Boardwalk','01291','869-908-1875','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',29),(2,'Lady McAnderson','2 Wonder Way','AF19B','11-445-910-2135','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',103),(3,'Dudley Do-Right','48 Horse Manor ','28198','874-916-2671','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',60);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `appointments` (
  `Appointment_ID` int NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Start` datetime DEFAULT NULL,
  `End` datetime DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Created_By` varchar(50) DEFAULT NULL,
  `Last_Update` timestamp NULL DEFAULT NULL,
  `Last_Updated_By` varchar(50) DEFAULT NULL,
  `Customer_ID` int NOT NULL,
  `User_ID` int NOT NULL,
  `Contact_ID` int NOT NULL,
  PRIMARY KEY (`Appointment_ID`),
  KEY `fk_customer_id_idx` (`Customer_ID`),
  KEY `fk_user_id_idx` (`User_ID`),
  KEY `fk_contact_id_idx` (`Contact_ID`),
  CONSTRAINT `fk_contact_id` FOREIGN KEY (`Contact_ID`) REFERENCES `contacts` (`Contact_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_customer_id` FOREIGN KEY (`Customer_ID`) REFERENCES `customers` (`Customer_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`User_ID`) REFERENCES `users` (`User_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,'title','description','location','Planning Session','2020-05-28 12:00:00','2020-05-28 13:00:00','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',1,1,3),(2,'title','description','location','De-Briefing','2020-05-29 12:00:00','2020-05-29 13:00:00','2021-09-10 13:14:10','script','2021-09-10 17:14:10','script',2,2,2);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;
