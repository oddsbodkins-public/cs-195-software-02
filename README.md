### An example business desktop application.

Written using the NetBeans Integrated development environment (IDE) version 15.
Java version 18.0.2.

Features
--------
* JavaFX 13. GUI application.
* JavaDoc comments. HTML documentation generator.
* Multi-platform / Operating System (OS). Written and Tested on Debian Linux 12 and Windows 10.
* Uses Java Database Connectivity (JDBC) to connect to a MariaDB / MySQL database. 
* User Authentication based on user table and rows.
* Internationalization (i18n) / Locationalization (l10n) with English and French labels, error messages, timezones, and strings.
* Create, Read, Update, and Delete (CRUD) Appointment table rows, Customer table rows, and Customer Contact Reports.
* Timezone tracking, timestamps, and datetime database columns.
* Appointment reminders based on user local timezone. Can be used by multiple users, even in different parts of the world.
* Application messages and error messages are logged to a file.
